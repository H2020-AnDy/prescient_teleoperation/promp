
/**
* Copyright: (C) 2020 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*/
#include "prompHandler.hpp"

using namespace std;
using namespace Eigen;
namespace fs = boost::filesystem;
const static IOFormat CSVFormat(FullPrecision, DontAlignCols, ", ", "\n");

PrompHandler::PrompHandler(){}

PrompHandler::PrompHandler(int promp_demos)
:promp_demos_(promp_demos){
}

#ifdef USE_PCA
PrompHandler::PrompHandler(int promp_demos, int data_dim, int encoded_dim)
:promp_demos_(promp_demos),data_dim_(data_dim),encoded_dim_(encoded_dim){
	pca.set_num_variables(data_dim_);
	mean_.resize(data_dim_);
}

void PrompHandler::init(int promp_demos, int data_dim, int encoded_dim){
	promp_demos_=promp_demos;
	data_dim_=data_dim;
	encoded_dim_=encoded_dim;
	pca.set_num_variables(data_dim_);
	mean_.resize(data_dim_);
}

Eigen::MatrixXd PrompHandler::loadPCADataset(string file_path){
	cout << "... Loading dataset" << endl;
    CSVReader reader(file_path);
    Eigen::MatrixXd qt_train = reader.getData();
    qt_train*=3.1415/180.0;

    return qt_train;
}

Eigen::VectorXd PrompHandler::getMeanPCADataset(){
	return mean_;
}

void PrompHandler::findPCAEncoding(const Eigen::MatrixXd &qt_train){
	for (int i = 0; i < qt_train.rows(); i++) {
        vector<double> record;
        for (int j=0; j<qt_train.cols(); j++){
        	record.push_back(qt_train(i,j));
        }
        pca.add_record(record);
    }
    cout << "... Solving PCA" << endl;
    pca.solve(); 
    cout << "[SUCCESS] PCA done" << endl;
    stringstream filePath;
	// get the current working directory
	fs::path pathfs = fs::current_path();
	string path = pathfs.string();
	filePath  << path <<  "/../etc/load/prompPred";
	string full_path  = filePath.str();
    pca.save(full_path);
}

void PrompHandler::loadPCA(){
	stringstream filePath;
	// get the current working directory
	fs::path pathfs = fs::current_path();
	string path = pathfs.string();
	filePath  << path <<  "/../etc/load/prompPred";
	string full_path  = filePath.str();
	pca.load(full_path);
	//get feature matrix
	for (int i=0; i<encoded_dim_; i++){
		FeatureM_.push_back(pca.get_eigenvector(i));
	}
	//transform in eigen matrix
	FeatureM.resize(FeatureM_.size(),FeatureM_[0].size());
	for (auto i = 0; i != FeatureM_.size(); ++i) {
		for (auto j = 0; j != FeatureM_[i].size(); ++j) {
	        FeatureM(i,j)=FeatureM_[i][j];
	    }
	}
	//get mean value
	vector<double> _mean = pca.get_mean_values();
	for (int i=0; i<_mean.size(); i++){
		mean_(i)=_mean[i];
	}
	cout << "[INFO] PCA data loaded" << endl;
}

MatrixXd PrompHandler::encodew_PCA(const MatrixXd &m_data){
	// Zero-mean centering for efficient PCA
	MatrixXd data_adjust(m_data.rows(),m_data.cols());
	for (int i=0; i<m_data.cols(); i++){
    	for (int j=0; j<m_data.rows(); j++){
    		data_adjust(j,i) = m_data(j,i) - mean_(i);
    	}
    }
	// Encode data. RowFeatureVector x RowDataAdjust
	return (FeatureM*(data_adjust.transpose())).transpose();
}

void PrompHandler::encode_train(string motion, string jbs){
	//encode demo wholebody trajectories using pca
	cout << "... Computing encoded demo data for ProMP" << endl;
	stringstream filePath;
	// get the current working directory
	fs::path pathfs = fs::current_path();
	string path = pathfs.string();
	filePath  << path <<  "/../etc/demos/";
	string full_path  = filePath.str();
	full_path += motion;
	if(jbs.compare("j")==0)
		full_path += "/";
	else
		full_path += "/p";
	for (int i=1; i<=promp_demos_; i++){
		CSVReader reader(full_path + to_string(i) + ".csv");
		MatrixXd q_demo = reader.getData(); //(timestep,data_dim_)
	    // For PCA to work properly, transform in radians
	    for (int i=0; i<q_demo.cols(); i++){
	    	for (int j=0; j<q_demo.rows(); j++){
	    		q_demo(j,i) = q_demo(j,i)*3.1415/180.0;
	    	}
	    }
	    MatrixXd encoded_demo = encodew_PCA(q_demo); //(timesteps,encoded_dim_)
	    if (!fs::exists(full_path)){
			fs::create_directory(full_path);
		}
		if(jbs.compare("j")==0)
	    	myfile.open(full_path + "/je" + to_string(i) + ".csv");
	    else
	    	myfile.open(full_path + "/pe" + to_string(i) + ".csv");
	    myfile << encoded_demo.format(CSVFormat);
		myfile.close();
	}
	cout << "[INFO] Encoded demo data for " << motion << endl;
}

void PrompHandler::learnEncodedPromp(string motion, string jbs, const int &n_rbf, const double &stdDev){
	cout << "... Loading encoded demo data for ProMP" << endl;
	vector<string> fileList;
	stringstream filePath;
	// get the current working directory
	fs::path pathfs = fs::current_path();
	string path = pathfs.string();
	filePath  << path <<  "/../etc/demos/";
	string full_path  = filePath.str();
	full_path += motion;
	for (int i=1; i<=promp_demos_; i++){
		if(jbs.compare("j")==0)
			fileList.push_back(full_path + "/je" + to_string(i) + ".csv");
		else
			fileList.push_back(full_path + "/pe" + to_string(i) + ".csv");
	}
	mean_alpha_=0;
	for (int i=0; i<encoded_dim_; i++){
		cout << "... Learning ProMP " << motion << " " << i+1 << "/" << encoded_dim_ << endl;
		//data handle object which takes an index of trajectory in .csv file and also the file list
		DataHandle promp_Data(i,fileList);
		// get required trajectory from all demonstrations into a single matrix
		MatrixXd data_Mp = promp_Data.getData();
		// get time modulation vector of the demo trajectories
		if(i==0){ //need it just once coz alpha_demos is the same for all joints
			vector<double> ad = promp_Data.getDeltaList();
			for (int j=0; j<ad.size(); j++){
				mean_alpha_ += ad[j];
			}
			mean_alpha_= mean_alpha_/ad.size();
		}
	    // initialize promp object with number of basis functions and std as arguments.
		ProMP m_proMp(data_Mp,n_rbf,stdDev);
		// Get mean and std deviation trajectories
		VectorXd m_Traj = m_proMp.generateTrajectory(1.0);
		VectorXd m_Std = m_proMp.genTrajStdDev();
		// Get weight vector and covariance matrix and stack them in a single matrix
		MatrixXd w(1,n_rbf);
		w.row(0) = m_proMp.getWeights();
		MatrixXd covW = m_proMp.getCovariance();
		// param row vector stdDev, #samples, and zeros
		MatrixXd param = MatrixXd::Zero(1,n_rbf);
		param(0,0)=stdDev;
		param(0,1)=m_proMp.getNoSamples();
		param(0,2)=mean_alpha_;
		MatrixXd promp_param(n_rbf+2,n_rbf);
		promp_param << w,
					   covW,
					   param;
	    // SAVING DATA			   
		stringstream filePath1;
		filePath1 << path <<  "/../etc/load/";
		full_path  = filePath1.str();
		full_path += motion;
		if (!fs::exists(full_path)){
			fs::create_directory(full_path);
		}	
		//Save in csv file the matrix that has as first row the weight vector and below the covariance matrix
		if(jbs.compare("j")==0)
			myfile.open(full_path + "/promp_JE" + to_string(i) + ".csv");
		else
			myfile.open(full_path + "/promp_PE" + to_string(i) + ".csv");
		myfile << promp_param.format(CSVFormat);
		myfile.close();
		// VISUALIZATION
		// Save mean and std deviation trajectories in csv files
		if(jbs.compare("j")==0)
			myfile.open(full_path + "/JE_" + to_string(i) + ".csv");
		else
			myfile.open(full_path + "/PE_" + to_string(i) + ".csv");
		// int j;
		int k;
		int plotlen;
		if (m_Traj.innerSize()>data_Mp.outerSize())
			plotlen =m_Traj.innerSize();
		else
			plotlen =data_Mp.outerSize();
		for (int i=0;i<plotlen;++i){
			// if (i<data_Mp.outerSize())
			// 	j=i;
			// else 
			// 	j=data_Mp.outerSize()-1;
			if (i<m_Traj.innerSize())
				k=i;
			else 
				k=m_Traj.innerSize()-1;
			myfile << m_Traj(k) << "," << m_Std(k) << endl;
		}
		myfile.close();
	}
	cout << "[SUCCESS] ProMP " << motion << " learned and saved" << endl;
}
#endif

Eigen::MatrixXd PrompHandler::retargetQTraj(Eigen::MatrixXd data){
	Eigen::MatrixXd data_ret;
	// XSENS-TO-ICUB
	data_ret.resize(data.rows(),32);
	data_ret.col(0) = data.col(5) + data.col(8) + data.col(11); 		//torso P
	data_ret.col(1) = -(data.col(3) + data.col(6) + data.col(9));   	//torso R
	data_ret.col(2) = data.col(5) + data.col(8) + data.col(11); 		//torso Y
	data_ret.col(3) = -data.col(17); 									//neck P
	data_ret.col(4) = data.col(15); 									//neck R
	data_ret.col(5) = data.col(16); 									//neck Y
	data_ret.col(6) = -data.col(35); 									//Lshoulder P
	data_ret.col(7) = data.col(33); 									//Lshoulder R
	data_ret.col(8) = data.col(34)-data.col(37); 						//Lshoulder Y
	data_ret.col(9) = data.col(38); 									//Lelbow
	data_ret.col(10) = data.col(40); 									//Lprosup
	data_ret.col(11) = -data.col(39); 									//Lwrist P
	data_ret.col(12) = -data.col(41); 									//Lwrist Y
	data_ret.col(13) = -data.col(23); 									//Rshoulder P
	data_ret.col(14) = data.col(21); 									//Rshoulder R
	data_ret.col(15) = data.col(22)-data.col(25); 						//Rshoulder Y
	data_ret.col(16) = data.col(26); 									//Relbow
	data_ret.col(17) = data.col(28); 									//Rprosup
	data_ret.col(18) = -data.col(27); 									//Rwrist P
	data_ret.col(19) = -data.col(29); 									//Rwrist Y
	data_ret.col(20) = data.col(56); 									//Lhip P
	data_ret.col(21) = data.col(54); 									//Lhip R
	data_ret.col(22) = -data.col(55); 									//Lhip Y
	data_ret.col(23) = -data.col(59); 									//Lknee
	data_ret.col(24) = -data.col(62); 									//Lankle P
	data_ret.col(25) = data.col(60); 									//Lankle R
	data_ret.col(26) = data.col(44); 									//Rhip P
	data_ret.col(27) = data.col(42); 									//Rhip R
	data_ret.col(28) = -data.col(43); 									//Rhip Y
	data_ret.col(29) = -data.col(47); 									//Rknee
	data_ret.col(30) = -data.col(50); 									//Rankle P
	data_ret.col(31) = data.col(48); 									//Rankle R

	return data_ret;
}

void PrompHandler::createRetargetedQTraj(string motion){
	cout << "... Loading demo data for ProMP" << endl;
	vector<string> fileList;
	stringstream filePath;
	// get the current working directory
	fs::path pathfs = fs::current_path();
	string path = pathfs.string();
	filePath  << path <<  "/../etc/demos/";
	string full_path = filePath.str();
	full_path += motion;
	for (int i=1; i<=promp_demos_; i++){
		CSVReader reader(full_path + "/" + to_string(i) + ".csv");
    	Eigen::MatrixXd m_data = reader.getData();
    	Eigen::MatrixXd m_data_ret = retargetQTraj(m_data);
    	
		myfile.open(full_path + "/r" + to_string(i) + ".csv");
		myfile << m_data_ret.format(CSVFormat);
		myfile.close();
	}
}

void PrompHandler::learnPromp(string motion, string jbs, const VectorXi &ID, const int &n_rbf, const double &stdDev){
	//load the joint data from the #promp_demos of the desired whole body motion
	cout << "... Loading demo data for ProMP" << endl;
	vector<string> fileList;
	stringstream filePath;
	// get the current working directory
	fs::path pathfs = fs::current_path();
	string path = pathfs.string();
	filePath  << path <<  "/../etc/demos/";
	string full_path  = filePath.str();
	full_path += motion;
	for (int i=1; i<=promp_demos_; i++){
		if(jbs.compare("j")==0)
			fileList.push_back(full_path + "/" + to_string(i) + ".csv");
		else if(jbs.compare("r")==0)
			fileList.push_back(full_path + "/r" + to_string(i) + ".csv");
		else
			fileList.push_back(full_path + "/p" + to_string(i) + ".csv");
	}
	cout << "[INFO] Demo loaded" << endl;
	mean_alpha_=0;
	for (int i=0; i<ID.size(); i++){
		cout << "... Learning ProMP " << jbs << " " << motion << " " << i+1 << "/" << ID.size() << endl;
		//data handle object which takes an index of trajectory in .csv file and also the file list
		DataHandle promp_Data(ID(i),fileList);
		// get required trajectory from all demonstrations into a single matrix
		MatrixXd data_Mp = promp_Data.getData();
		// get time modulation vector of the demo trajectories
		if(i==0){ //need it just once coz alpha_demos is the same for all joints
			vector<double> ad = promp_Data.getDeltaList();
			for (int j=0; j<ad.size(); j++){
				mean_alpha_ += ad[j];
			}
			mean_alpha_= mean_alpha_/ad.size();
		}	
	    // initialize promp object with number of basis functions and std as arguments.
		ProMP m_proMp(data_Mp,n_rbf,stdDev);
		// Get mean and std deviation trajectories
		VectorXd m_Traj = m_proMp.generateTrajectory(1.0);
		VectorXd m_Std = m_proMp.genTrajStdDev();
		// Get weight vector and covariance matrix and stack them in a single matrix
		MatrixXd w(1,n_rbf);
		w.row(0) = m_proMp.getWeights();
		MatrixXd covW = m_proMp.getCovariance();
		// param row vector stdDev, #samples, and zeros
		MatrixXd param = MatrixXd::Zero(1,n_rbf);
		param(0,0)=stdDev;
		param(0,1)=m_proMp.getNoSamples();
		param(0,2)=mean_alpha_;
		MatrixXd promp_param(n_rbf+2,n_rbf);
		promp_param << w,
					   covW,
					   param;
	    // SAVING DATA			   
		stringstream filePath1;
		// get the current working directory
		filePath1 << path <<  "/../etc/load/";
		full_path  = filePath1.str();
		full_path += motion;
		if (!fs::exists(full_path)){
			fs::create_directory(full_path);
		}	
		//Save in csv file the matrix that has as first row the weight vector and below the covariance matrix
		// last row: stdDev, #samples, and zeros
		if(jbs.compare("j")==0)
			myfile.open(full_path + "/promp_J" + to_string(ID(i)) + ".csv");
		else if(jbs.compare("r")==0)
			myfile.open(full_path + "/promp_R" + to_string(ID(i)) + ".csv");
		else
			myfile.open(full_path + "/promp_P" + to_string(ID(i)) + ".csv");
		myfile << promp_param.format(CSVFormat);
		myfile.close();
		// VISUALIZATION
		// Save mean and std deviation trajectories in csv files
		if(jbs.compare("j")==0)
			myfile.open(full_path + "/J_" + to_string(ID(i)) + ".csv");
		else if(jbs.compare("r")==0)
			myfile.open(full_path + "/R_" + to_string(ID(i)) + ".csv");
		else
			myfile.open(full_path + "/P_" + to_string(ID(i)) + ".csv");
		// int j;
		int k;
		int plotlen;
		if (m_Traj.innerSize()>data_Mp.outerSize())
			plotlen =m_Traj.innerSize();
		else
			plotlen =data_Mp.outerSize();
		for (int i=0;i<plotlen;++i){
			// if (i<data_Mp.outerSize())
			// 	j=i;
			// else 
			// 	j=data_Mp.outerSize()-1;
			if (i<m_Traj.innerSize())
				k=i;
			else 
				k=m_Traj.innerSize()-1;
			myfile << m_Traj(k) << "," << m_Std(k) << endl;
		}
		myfile.close();
	}
	cout << "[SUCCESS] ProMP " << motion << " learned and saved" << endl;
}

void PrompHandler::learnPromp(string motion, string jbs, int ID, const int &n_rbf, const double &stdDev){
	//load the joint data from the #promp_demos of the desired whole body motion
	cout << "... Loading demo data for ProMP" << endl;
	vector<string> fileList;
	stringstream filePath;
	// get the current working directory
	fs::path pathfs = fs::current_path();
	string path = pathfs.string();
	filePath  << path <<  "/../etc/demos/";
	string full_path  = filePath.str();
	full_path += motion;
	for (int i=1; i<=promp_demos_; i++){
		if(jbs.compare("j")==0)
			fileList.push_back(full_path + "/" + to_string(i) + ".csv");
		else if(jbs.compare("r")==0)
			fileList.push_back(full_path + "/r" + to_string(i) + ".csv");
		else
			fileList.push_back(full_path + "/p" + to_string(i) + ".csv");
	}
	cout << "[INFO] Demo loaded" << endl;
	mean_alpha_=0;
	//data handle object which takes an index of trajectory in .csv file and also the file list
	DataHandle promp_Data(ID,fileList);
	// get required trajectory from all demonstrations into a single matrix
	MatrixXd data_Mp = promp_Data.getData();
	// get time modulation vector of the demo trajectories
	vector<double> ad = promp_Data.getDeltaList();
	for (int j=0; j<ad.size(); j++){
		mean_alpha_ += ad[j];
	}
	mean_alpha_= mean_alpha_/ad.size();
    // initialize promp object with number of basis functions and std as arguments.
	ProMP m_proMp(data_Mp,n_rbf,stdDev);
	// Get mean and std deviation trajectories
	VectorXd m_Traj = m_proMp.generateTrajectory(1.0);
	VectorXd m_Std = m_proMp.genTrajStdDev();
	// Get weight vector and covariance matrix and stack them in a single matrix
	MatrixXd w(1,n_rbf);
	w.row(0) = m_proMp.getWeights();
	MatrixXd covW = m_proMp.getCovariance();
	// param row vector stdDev, #samples, and zeros
	MatrixXd param = MatrixXd::Zero(1,n_rbf);
	param(0,0)=stdDev;
	param(0,1)=m_proMp.getNoSamples();
	param(0,2)=mean_alpha_;
	MatrixXd promp_param(n_rbf+2,n_rbf);
	promp_param << w,
				   covW,
				   param;
    // SAVING DATA			   
	stringstream filePath1;
	// get the current working directory
	filePath1 << path <<  "/../etc/load/";
	full_path  = filePath1.str();
	full_path += motion;
	if (!fs::exists(full_path)){
		fs::create_directory(full_path);
	}	
	//Save in csv file the matrix that has as first row the weight vector and below the covariance matrix
	// last row: stdDev, #samples, and zeros
	if(jbs.compare("j")==0)
		myfile.open(full_path + "/promp_J" + to_string(ID) + ".csv");
	else if(jbs.compare("r")==0)
		myfile.open(full_path + "/promp_R" + to_string(ID) + ".csv");
	else
		myfile.open(full_path + "/promp_P" + to_string(ID) + ".csv");
	myfile << promp_param.format(CSVFormat);
	myfile.close();
	// VISUALIZATION
	// Save mean and std deviation trajectories in csv files
	if(jbs.compare("j")==0)
		myfile.open(full_path + "/J_" + to_string(ID) + ".csv");
	else if(jbs.compare("r")==0)
		myfile.open(full_path + "/R_" + to_string(ID) + ".csv");
	else
		myfile.open(full_path + "/P_" + to_string(ID) + ".csv");
	// int j;
	int k;
	int plotlen;
	if (m_Traj.innerSize()>data_Mp.outerSize())
		plotlen =m_Traj.innerSize();
	else
		plotlen =data_Mp.outerSize();
	for (int i=0;i<plotlen;++i){
		// if (i<data_Mp.outerSize())
		// 	j=i;
		// else 
		// 	j=data_Mp.outerSize()-1;
		if (i<m_Traj.innerSize())
			k=i;
		else 
			k=m_Traj.innerSize()-1;
		myfile << m_Traj(k) << "," << m_Std(k) << endl;
	}
	myfile.close();
	cout << "[SUCCESS] ProMP " << motion << " learned and saved" << endl;
}

std::vector<ProMP> PrompHandler::getPromp(string motion, string jbs, const bool &encoded){
	stringstream filePath;
	// get the current working directory
	fs::path pathfs = fs::current_path();
	string path = pathfs.string();
	filePath  << path <<  "/../etc/load/";
	string full_path  = filePath.str();
	full_path += motion;
	int size;
	if (encoded){
		if(jbs.compare("j")==0)
			full_path += "/promp_JE";
		else
			full_path += "/promp_PE";
		size = encoded_dim_;
	}
	else{
		if(jbs.compare("j")==0)
			full_path += "/promp_J";
		else if(jbs.compare("r")==0)
			full_path += "/promp_R";
		else
			full_path += "/promp_P";
		size = data_dim_;
	}
	std::vector<ProMP> n_proMp;
	for (int i=0; i<size; i++){
		CSVReader reader(full_path + to_string(i) + ".csv");
		MatrixXd promp_data = reader.getData(); //(n_rbf+2,n_rbf)
		VectorXd w_ = promp_data.row(0);
		int last_row = promp_data.rows()-1;
		VectorXd param_ = promp_data.row(last_row);
		Eigen::MatrixXd covW_ = promp_data.block(1,0,w_.size(),w_.size());
		ProMP oneD_promp(w_,covW_,w_.size(),param_(0),param_(1),param_(2));
		n_proMp.push_back(oneD_promp);
	}

	return n_proMp;
}


ProMP PrompHandler::getPromp(string motion, string jbs, int id, const bool &encoded){
	stringstream filePath;
	// get the current working directory
	fs::path pathfs = fs::current_path();
	string path = pathfs.string();
	filePath  << path <<  "/../etc/load/";
	string full_path  = filePath.str();
	full_path += motion;
	int size;
	if (encoded){
		if(jbs.compare("j")==0)
			full_path += "/promp_JE";
		else 
			full_path += "/promp_PE";
	}
	else{
		if(jbs.compare("j")==0)
			full_path += "/promp_J";
		else if(jbs.compare("r")==0)
			full_path += "/promp_R";
		else
			full_path += "/promp_P";
	}
	CSVReader reader(full_path + to_string(id) + ".csv");
	MatrixXd promp_data = reader.getData(); //(n_rbf+2,n_rbf)
	VectorXd w_ = promp_data.row(0);
	int last_row = promp_data.rows()-1;
	VectorXd param_ = promp_data.row(last_row);
	Eigen::MatrixXd covW_ = promp_data.block(1,0,w_.size(),w_.size());
	ProMP oneD_promp(w_,covW_,w_.size(),param_(0),param_(1),param_(2));

	return oneD_promp;
}


double PrompHandler::computeMotionThreshold(string motion, string jbs, int id, const bool &encoded, int obs_duration){
	cout << "[INFO] Computing ProMP classificaton threshold for " << motion << endl;
	std::vector<double> distance;
	double dist=0;
	fs::path pathfs = fs::current_path();
	string path = pathfs.string();
	//get demo trajectories
	stringstream filePath;
	filePath  << path <<  "/../etc/demos/";
	string full_path  = filePath.str();
	full_path += motion;
	full_path += "/";
	if (jbs.compare("j")!=0)
		full_path += jbs;
	if(encoded){
		if(jbs.compare("j")==0)
			full_path += "j";
		full_path += "e";
	}
	std::vector<std::string> fileList;
	for (int j=1; j<=promp_demos_; j++){
		fileList.push_back(full_path + to_string(j) + ".csv");
	}
	DataHandle dh(id,fileList);
	Eigen::MatrixXd demo_data = dh.getData();
	// get promp of motion
	ProMP promp= getPromp(motion,jbs,id,encoded);
	// compute mean trajectory
	Eigen::VectorXd meanTraj = promp.generateTrajectory((double)(promp.getNoSamples())/(demo_data.cols()));
	// compute distance - derivates demo traj and mean traj of promp
	if(obs_duration>meanTraj.size()){
		obs_duration=meanTraj.size()-1;
		cout << "[WARNING] obs_duration > trajectory length of ProMP " << motion << endl;
	}
	for (int r=0; r<demo_data.rows(); r++){
		// select trajectory corresponding to id
		Eigen::VectorXd demoTraj = demo_data.row(r);
		for (int i=0; i<obs_duration; i++){
			dist += abs(demoTraj(i) - meanTraj(i));
		}
		distance.push_back(dist);
		dist=0;
	}
	return *std::max_element(distance.begin(), distance.end());
}

void PrompHandler::saveMotionsThreshold(vector<double> threshold_vec){
	Eigen::VectorXd threshold_eig;
	int size = threshold_vec.size();
	threshold_eig.resize(size);
	for (int i=0; i<size; i++){
		threshold_eig(i)=threshold_vec[i];
	}
	//save to file
	fs::path pathfs = fs::current_path();
	stringstream filePath;
	string path = pathfs.string();
	filePath  << path <<  "/../etc/load/";
	string full_path  = filePath.str();
	myfile.open(full_path + "threshold.csv");
	myfile << threshold_eig.format(CSVFormat);
	myfile.close();
	cout << "[SUCCESS] Threshold computed and saved" << endl;
}


Eigen::VectorXd PrompHandler::getMotionsThreshold(){
	fs::path pathfs = fs::current_path();
	stringstream filePath;
	string path = pathfs.string();
	filePath  << path <<  "/../etc/load/threshold.csv";
	string full_path  = filePath.str();
	CSVReader reader(full_path);
	return (reader.getData().col(0));
}