#include "csvReader.hpp"

CSVReader::CSVReader(std::string filename, std::string delm, bool skipHeader ):fileName_(filename), delimeter_(delm), skipHeader_(skipHeader){
	//just initialize attributes
}

Eigen::MatrixXd CSVReader::getData(){
	std::ifstream file(fileName_.c_str()); 
	if (!file.is_open()){
		std::cout<<"Error reading .csv file. Check File path."<<std::endl;
		exit(1);
		}

	std::string line = "";
	while (getline(file, line)){
		if (dataList_.size() == 0 && skipHeader_){
			skipHeader_ = false;
			continue;
		}
		std::vector<std::string> vec;
		boost::algorithm::split(vec, line, boost::is_any_of(delimeter_));
		dataList_.push_back(vec);
		}
	file.close();	
	for(std::vector<std::string> vec : dataList_){
		std::vector<double> oneLine;
		for(std::string data : vec){
			try{ std::stod(data);
				}
			catch (...){
				std::cerr<<"invalid elements inside "<<fileName_<<". Check if all elements are doubles."<<std::endl;
				exit(1);
				}
			oneLine.push_back(std::stod(data));
			}
		list.push_back(oneLine);		
		}
	Eigen::MatrixXd data(list.size(),list[0].size());

	for (int row = 0; row < list.size(); ++row){
			for (int col = 0; col < list[0].size(); ++col){
	    		data(row,col) = list[row][col];
			}
	}
	return data;
}

Eigen::VectorXd CSVReader::get1Ddata(int index,Eigen::MatrixXd data){
	Eigen::VectorXd data_aux(list.size());

	if (index<0 || index > data.outerSize()){
		std::cout<<"Indexing error in .csv file. Index= columns in .csv file. Indexing starts from 0."<<std::endl;
		exit(1);
	}

	for (int row = 0; row < data_aux.innerSize(); ++row){   		
        	data_aux(row) = data(row,index) ;		
	}

	return data_aux;
}
