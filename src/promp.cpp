#include "promp.hpp"

ProMP::ProMP(Eigen::MatrixXd& data,int numBf, double stdBf):numBf_(numBf),stdBf_(stdBf){
	numDemos_ = data.innerSize();
	s_ = data.outerSize(); 
	computePhase();
	phi_ = generateBasisFunction(phase_);
	train(data);
}

ProMP::ProMP( std::vector<std::string> fileList, int dataColumn, int numBf, double stdBf):numBf_(numBf),stdBf_(stdBf){
	DataHandle myData(dataColumn,fileList);
	Eigen::MatrixXd data = myData.getData(); 
	numDemos_ = data.innerSize();
	s_ = data.outerSize();
	computePhase();
	phi_ = generateBasisFunction(phase_);
	train(data);
}

ProMP::ProMP(Eigen::VectorXd w, Eigen::MatrixXd covW, int numBf, double stdBf, int n_sample, double m_alpha)
:meanW_(w),
covW_(covW),
numBf_(numBf),
stdBf_(stdBf),
s_(n_sample),
m_alpha_(m_alpha){}

ProMP::ProMP(Eigen::VectorXd w, Eigen::MatrixXd covW, int numBf, double stdBf, int n_sample)
:meanW_(w),
covW_(covW),
numBf_(numBf),
stdBf_(stdBf),
s_(n_sample){}

ProMP::ProMP(Eigen::VectorXd w, Eigen::MatrixXd covW, int numBf, double stdBf, int n_sample, double alpha, Eigen::VectorXd phase, Eigen::MatrixXd phi)
:meanW_(w),
covW_(covW),
numBf_(numBf),
stdBf_(stdBf),
s_(n_sample),
alpha_(alpha),
phase_(phase),
phi_(phi){}

ProMP ProMP::operator- () const{
	auto phi_t = phi_.transpose();

	auto copyWeights = -(phi_*phi_t).inverse()*phi_*phi_t* meanW_;
	ProMP copyMP = *this;
	copyMP.setWeights(copyWeights);


	return copyMP;
}

Eigen::VectorXd ProMP::linspace(double start, double end, int number){
	Eigen::VectorXd result(number);
	double factor = (end - start)/(number -1);
	for (int i=0;i<number;++i){
		result(i)=i*factor;
	}
	return result;
}

void ProMP::computePhase(){
	double dt =1.0/s_;
	int numTimeSteps = (int)(s_/alpha_);

	Eigen::VectorXd dz = Eigen::RowVectorXd::Ones(numTimeSteps)*alpha_;
	Eigen::VectorXd z = cumSum(dz)*(dt);
	phase_ = z;
}

Eigen::VectorXd ProMP::cumSum(Eigen::VectorXd vect){
	Eigen::VectorXd sumArr(vect.size());
	double acc = 0;
	// #pragma omp parallel
	// {
		for(int i = 0; i < vect.size(); i++){
	 		acc += vect(i);
	 		sumArr(i) = acc;
		}
	// }
	return sumArr;
}

Eigen::VectorXd ProMP::getPhaseFromTime(int timePos){
	Eigen::VectorXd phaseN(1);
	//TODO Check for alpha!=1, approximate s_ to multiple of alpha_
	int s_appr = (int)(s_/alpha_);
	phaseEnd_ = s_appr-1;
	phaseN(0) = (double) timePos / (double) phaseEnd_;
	return phaseN;
}

Eigen::VectorXd ProMP::getPhase(){
	return phase_;
}

void ProMP::setPhase(Eigen::VectorXd z){
	phase_ = z;
}

void ProMP::computePhaseForSteps(int reqNumberSteps){
	double speed = (double) (s_/(double)reqNumberSteps);
	// std::cout << "speed "<< speed;
	setTimeMod(speed);
	computePhase();
}

Eigen::MatrixXd ProMP::generateBasisFunction(const Eigen::VectorXd &phase){
	// create vector of rbf centers
	c_ = linspace(0,1,numBf_);
	// create a matrix with n_rbf rows phase.size() cols. each row is the phase
	Eigen::MatrixXd phaseBfMat = phase.replicate(1,numBf_).transpose();	
	//create a matrix with n_rbf rows and phase.size() cols. each col is c_
	Eigen::MatrixXd cMat = c_.replicate(1,phase.rows());
	// compute t-c(i)
	Eigen::MatrixXd phaseDiff = phaseBfMat - cMat;
	//compute rbf matrix. not normalized yet
	Eigen::MatrixXd phi_temp = rbfGaussian(phaseDiff).transpose();
	// compute sum of each basis function phi_(i) (i=0,...,n_rbf)
	Eigen::VectorXd sumBf = phi_temp.rowwise().sum();
	// normalize the rbfs
	Eigen::MatrixXd phi(phase.rows(),numBf_);
	// #pragma omp parallel
	// {
		for (int i=0; i< phase.rows() ;++i){
			for (int j=0; j< numBf_; ++j){
				phi(i,j)= phi_temp(i,j)/sumBf(i);
			}
		}
	// }
	return phi.transpose();
}

Eigen::MatrixXd ProMP::getBasisFunction(){
	return phi_;
}

Eigen::VectorXd ProMP::getWeights(){
	return meanW_;
}

Eigen::MatrixXd ProMP::getCovariance(){
	return covW_;
}

void ProMP::setCovariance(const Eigen::MatrixXd& cov){
	covW_ = cov;
}

double ProMP::getStdBf(){
	return stdBf_;
}

int ProMP::getNoSamples(){
	return s_;
}

void ProMP::setNoSamples(int n_sample){
	s_ = n_sample;
}

int ProMP::getTrajLength(){
	int trajLength = (int)(s_/alpha_);
	return trajLength;
}

double ProMP::getTimeMod(){
	return alpha_;
}

double ProMP::getMeanDemoTimeMod(){
	m_alpha_ *=10;
	m_alpha_=trunc(m_alpha_)*0.1;
	return m_alpha_;
}

void ProMP::setTimeMod(double timeMod){
	alpha_ = timeMod;
}

void ProMP::train(Eigen::MatrixXd& dataBase){
	// double ridge_factor = 0.0000000001;
	double ridge_factor = 0.0001;
	// double ridge_factor = 0.0;
	Eigen::MatrixXd W_(numDemos_,numBf_);
	// linear ridge regression for each demonstration
	for (int i=0; i<dataBase.innerSize();++i){
		auto phi_T = phi_.transpose(); 
		auto eye = MatrixXd::Identity(numBf_,numBf_);
		auto wDemoTraj = (((phi_*phi_T+ridge_factor*eye).inverse())*(phi_*dataBase.row(i).transpose())).transpose();
		// auto wDemoTraj = phi_T.bdcSvd(ComputeThinU | ComputeThinV).solve(dataBase.row(i).transpose()).transpose(); //solving with SVD
		// auto wDemoTraj = phi_T.colPivHouseholderQr().solve(dataBase.row(i).transpose()).transpose(); //solving with QR
		//for some reason this version of the equation is faster to compute than the original one from the paper (same results)
		W_.row(i) = wDemoTraj;
	}

	Eigen::VectorXd meanW = W_.colwise().mean();
	meanW_ = meanW;
	Eigen::MatrixXd covMat = W_;
	MatrixXd centered = covMat.rowwise() - covMat.colwise().mean();
	MatrixXd cov = (centered.adjoint() * centered) / double(covMat.rows() - 1);
	covW_ = cov; 

	// for average trajectory
	means_ = dataBase.colwise().mean();
	start_ = means_(0);
	end_   = means_(s_-1);
}

void ProMP::addViaPoint(double t, double viaPoint, double std){
	//todo: verify if via point has already been inserted
	Eigen::Vector3d viaPoint_;
	viaPoint_(0) = t;
	viaPoint_(1) = viaPoint;
	viaPoint_(2) = std;
	viaPoints_.push_back(viaPoint_);
}

void ProMP::setStart(double start,double std){
	addViaPoint(0.0,start,std);
}

void ProMP::setGoal(double goal,double std){
	addViaPoint(1.0,goal,std);	
}


Eigen::VectorXd ProMP::generateTrajectory(int reqNumberSteps){
	//calculate phase speed and new phase object for required number of steps
	computePhaseForSteps(reqNumberSteps);

	// generate basis function for new phase parameterization
	auto phi = generateBasisFunction(phase_);

	//compute trajectory from weights and new phase
	auto traj = phi.transpose()*meanW_;

	return traj;
}

Eigen::VectorXd ProMP::generateTrajectory(double reqPhaseSpeed){
	setTimeMod(reqPhaseSpeed);
	//modulate speed
	computePhase();
	// generate basis function for new phase parameterization
	auto phi = generateBasisFunction(phase_);
	//compute trajectory from weights and new phase
	auto traj = phi.transpose()*meanW_;

	return traj;
}

Eigen::VectorXd ProMP::generateTrajectory(){
	//compute trajectory from weights and basis function
	return phi_.transpose()*meanW_;
}

Eigen::VectorXd ProMP::generateTrajectory(Eigen::VectorXd weights,int requiredTrajLen){
	//calculate phase speed and new phase object for required number of steps
	computePhaseForSteps(requiredTrajLen);

	// generate basis function for new phase parameterization
	auto phi = generateBasisFunction(phase_);

	//compute trajectory from weights and new phase
	auto traj = phi.transpose()*weights;

	return traj;
}

Eigen::VectorXd ProMP::generateTrajectory(Eigen::VectorXd weights,double reqPhaseSpeed){
	setTimeMod(reqPhaseSpeed);
	//modulate speed
	computePhase();
	// generate basis function for new phase parameterization
	auto phi = generateBasisFunction(phase_);

	//compute trajectory from weights and new phase
	auto traj = phi.transpose()*weights;

	return traj;
}

Eigen::VectorXd ProMP::genTrajStdDev(){
	//gets current basis function based on current phase parameterization of time
	Eigen::MatrixXd phi = generateBasisFunction(phase_);
	Eigen::MatrixXd phi_T = phi.transpose();
	//std::cout << "phi: " <<  phi << std::endl;
	Eigen::MatrixXd trajStd = phi_T*covW_*phi;

	Eigen::MatrixXd obsErrorCov = MatrixXd::Identity(trajStd.rows(), trajStd.rows()) * OBS_ERROR;

	trajStd = trajStd + obsErrorCov;
	trajStd = trajStd.cwiseSqrt();

	return trajStd.diagonal();
}

Eigen::VectorXd ProMP::genTrajStdDev(int reqNumberSteps){
	//calculate phase speed and new phase object for required number of steps
	computePhaseForSteps(reqNumberSteps);

	// generate basis function for new phase parameterization
	auto phi = generateBasisFunction(phase_);

	Eigen::MatrixXd trajStd = phi.transpose()*covW_*phi;

	Eigen::MatrixXd obsErrorCov = MatrixXd::Identity(trajStd.rows(), trajStd.rows()) * OBS_ERROR;

	trajStd = trajStd + obsErrorCov;
	trajStd = trajStd.cwiseSqrt();

	return trajStd.diagonal();
}


void ProMP::conditionViaPoints(void){
	//loop var initialization
	auto newMeanW = meanW_;
	auto newCovW = covW_;

	for(int i=0;i<viaPoints_.size();++i){
		auto phase_obs = getPhaseFromTime(viaPoints_[i][0]);
		auto phi_obs = generateBasisFunction(phase_obs);

		Eigen::VectorXd obs(1);
		obs(0) = viaPoints_[i][1];

		Eigen::VectorXd sig_obs(1);
		sig_obs(0) = viaPoints_[i][2];

		auto L = covW_*phi_obs*(sig_obs+phi_obs.transpose()*covW_*phi_obs).inverse();
		
		newMeanW = newMeanW + L*(obs-phi_obs.transpose()*newMeanW);
		newCovW = newCovW -L*phi_obs.transpose()*newCovW;
	}
	//update weights
	covW_ = newCovW;
	meanW_ = newMeanW;
}

Eigen::VectorXd ProMP::generateAvgTrajectory(){
	setStart(means_(0),0.000001);
	setGoal(means_(s_-1),0.000001);
	conditionViaPoints();
	return generateTrajectory(s_);
}


Eigen::MatrixXd ProMP::rbfGaussian(Eigen::MatrixXd& phaseDiff){
	Eigen::MatrixXd phaseDiffF(phaseDiff.innerSize(),phaseDiff.outerSize());
	for(int i=0;i<phaseDiff.innerSize();++i){
		for(int j=0;j<phaseDiff.outerSize();++j){
			auto x = phaseDiff(i,j);
			// phaseDiffF(i,j) = std::exp(-0.5*pow(x/stdBf_,2))/(stdBf_);
			// phaseDiffF(i,j) = std::exp(-0.5*x*x/stdBf_);
			phaseDiffF(i,j) = std::exp(-0.5*pow(x/stdBf_,2))/(sqrt(2*M_PI)*stdBf_);
		}
	}
	return phaseDiffF;
}

double ProMP::getAvgStart(){
	return start_;
}

double ProMP::getAvgEnd(){
	return end_;
}

// TODO: add a try-catch here to prevent the user from setting invalid sized weight sets
void ProMP::setWeights(const Eigen::VectorXd& w){
	int curSize = meanW_.size();
	if(curSize != w.size()){
		std::cout << "[ProMP]: The new set of weights has a different dimension than the current one.\n";
		std::cout << "[ProMP]: Desired: "<<w.size()<<" Current: "<<curSize<< "\n";
		std::cout << "[ProMP]: Ignoring setWeights request...\n";
		throw std::runtime_error("[ProMP]: The new set of weights has a different dimension than the current one");
		return;
	}
	meanW_ = w;

}



void ProMP::computeDelta(double K){
	double ridge_factor = 0.000000000001;
	// double ridge_factor = 0.0;

	auto phi_T = phi_.transpose(); 
	auto eye = MatrixXd::Identity(numBf_,numBf_);
	phiInv_ = (((phi_*phi_T+ridge_factor*eye).inverse())*(phi_));
	delta_wy_ = phiInv_*genTrajStdDev()*K;
}

Eigen::VectorXd ProMP::getUpperWeights(double K){
	computeDelta(K);
	return meanW_ + delta_wy_;

}

Eigen::VectorXd ProMP::getLowerWeights(double K){
	computeDelta(K);
	return meanW_ - delta_wy_;	
}

void ProMP::setId(std::string id){
	id_ = id;
}

std::string ProMP::getId(void){
	return id_;
}

std::vector<double> ProMP::getMeanWeightsFromList(std::vector<ProMP> mpList){
    
	// loop through the list
    std::vector<double> allWeights(0);
    for(auto& mp: mpList){
        
        Eigen::VectorXd eigWeights = mp.getWeights();
        std::vector<double> stdWeights;

        // convert to std::vector
        // api::eigenToStdVec(eigWeights, stdWeights);
		stdWeights.resize(eigWeights.size());
		Eigen::VectorXd::Map(stdWeights.data(), stdWeights.size()) = eigWeights;

		
        // concatenate weights vec
        allWeights.insert(allWeights.end(), stdWeights.begin(), stdWeights.end());
    }

    return allWeights;

}

ostream & operator << (ostream &out, ProMP &mp){
	out<<"The weights of "<< mp.getId() << " are: ";

	auto weights = mp.getWeights();
	for(int i=0;i< weights.size();i++){
		out << weights[i] << ", ";
	}
	// out << "\n";

	return out;
}
