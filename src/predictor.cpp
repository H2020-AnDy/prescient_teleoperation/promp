/**
* Copyright: (C) 2020 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*/
#include "predictor.hpp"

Predictor::Predictor(double dataNoise):expNoise_(dataNoise){}

double Predictor::predictTimeMod(const Eigen::VectorXd &obsTraj, ProMP promp, double lb, double ub, double step){
	std::vector<double> alphas;
	for (double i=lb; i<=ub; i+=step){
		alphas.push_back(i);
	}
	std::vector<double> distance;
	double dist=0;
	// Find the alpha that minimizes the distance between the observed trajectory and the mean trajectory of the ProMP. 
	for (int i=0; i<alphas.size(); i++){
		// compute mean trajectory given a certain alpha
		Eigen::VectorXd meanTraj = promp.generateTrajectory(alphas[i]);
		if (meanTraj.size()<obsTraj.size()){
			// std::cout << "[WARNING] Mean trajectory of ProMP is shorter than the observed portion of trajectory!" << std::endl;
			// std::cout << "[WARNING] The upper bound (ub) for alpha in Predictor::predictTimeMod is not reached" << std::endl;
			// std::cout << "Mean size: " << meanTraj.size() << " - Obs size: " <<  obsTraj.size() << std::endl;
			break;
		}
		// compute distance - observed traj and mean traj of promp
		#pragma omp parallel for 
		for (int k=0; k<obsTraj.size(); k++){
			dist += abs(obsTraj(k) - meanTraj(k));
		}
		distance.push_back(dist);
		dist=0;
	}
	int minDistanceIndex = std::min_element(distance.begin(),distance.end()) - distance.begin();
	double timeMod = alphas[minDistanceIndex];

	return timeMod;
}

double Predictor::predictTimeMod(std::vector<Eigen::VectorXd> obsTraj, std::vector<ProMP> promp, double lb, double ub, double step){
	std::vector<double> alphas;
	for (double i=lb; i<=ub; i+=step){
		alphas.push_back(i);
	}
	std::vector<double> distance;
	double dist=0;
	// Find the alpha that minimizes the distance between the observed trajectory and the mean trajectory of the ProMP. 
	for (int i=0; i<alphas.size(); i++){
		std::vector<Eigen::VectorXd> meanTraj;
		// compute mean trajectory given a certain alpha
		for (int j=0; j<promp.size(); j++){
			meanTraj.push_back(promp[j].generateTrajectory(alphas[i]));
		}
		if (meanTraj[0].size()<obsTraj[0].size()){
			// std::cout << "[WARNING] Mean trajectory of ProMP is shorter than the observed portion of trajectory!" << std::endl;
			// std::cout << "[WARNING] The upper bound (ub) for alpha in Predictor::predictTimeMod is not reached" << std::endl;
			// std::cout << "Mean size: " << meanTraj.size() << " - Obs size: " <<  obsTraj.size() << std::endl;
			break;
		}
		// compute derivate mean traj of promp
		Eigen::VectorXd dmTraj;
		Eigen::VectorXd doTraj;
		dmTraj.resize(obsTraj[0].size()-1);
		doTraj.resize(obsTraj[0].size()-1);
		// compute distance - observed traj and mean traj of promp
		int size;
		// #pragma omp parallel for 
		for (int n=0; n<obsTraj.size(); n++){
			if(obsTraj[n].size()<meanTraj[n].size())
				size = obsTraj[n].size()-1;
			else 
				size = meanTraj[n].size()-1;
			for (int k=0; k<size; k++){	
				dmTraj(k) = (meanTraj[n])(k+1)-(meanTraj[n])(k);
				doTraj(k) = (obsTraj[n])(k+1)-(obsTraj[n])(k);
				dist += abs(doTraj(k) - dmTraj(k));
			}
		}
		distance.push_back(dist);
		dist=0;
	}
	int minDistanceIndex = std::min_element(distance.begin(),distance.end()) - distance.begin();
	double timeMod = alphas[minDistanceIndex];

	return timeMod;
}


int Predictor::inferProMP(const Eigen::VectorXd &obsTraj, std::vector<ProMP> listPromp, const std::vector<double> &infrdTimeMod, const Eigen::VectorXd &infr_threshold){
	int idx=0;
	std::vector<double> distance;
	double dist=0;
	// For each ProMP in the ProMp list, find the distance between the observed trajectory and the mean trajectory of the ProMP w/ inferred time modulation. 
	for (int i=0; i<listPromp.size(); i++)
	{
		// compute mean trajectory given predicted alpha
		Eigen::VectorXd meanTraj = listPromp[i].generateTrajectory(infrdTimeMod[idx]);
		if (meanTraj.size()<obsTraj.size()){
			std::cout << "[ERROR] Hint: Check bounds of alpha when calling Predictor::predictTimeMod or check length of obsTraj in Predictor::inferProMP" << std::endl;
			return -1;
		}
		idx++;

		// compute distance - derivates observed traj and mean traj of promp
		#pragma omp parallel for
		for (int i=0; i<obsTraj.size(); i++){
			dist += abs(obsTraj(i) - meanTraj(i));
		}
		distance.push_back(dist);
		// std::cout << dist << std::endl;
		dist=0;
	}
	int infrd_id = std::min_element(distance.begin(),distance.end()) - distance.begin();
	if (distance[infrd_id]>infr_threshold[infrd_id])
		return -1;
	else
		return infrd_id;
}


int Predictor::inferProMP(std::vector<Eigen::VectorXd> obsTraj, std::vector<std::vector<ProMP>> listPromp, const std::vector<double> &infrdTimeMod, const Eigen::VectorXd &infr_threshold){
	int idx=0;
	std::vector<double> distance;
	double dist=0;
	// For each ProMP in the ProMp list, find the distance between the observed trajectory and the mean trajectory of the ProMP w/ inferred time modulation. 
	for (int i=0; i<listPromp[0].size(); i++)
	{
		std::vector<Eigen::VectorXd> meanTraj;
		for (int j=0; j<listPromp.size(); j++){
			// compute mean trajectory given predicted alpha
			meanTraj.push_back(listPromp[j][i].generateTrajectory(infrdTimeMod[idx]));
		}
		if (meanTraj[0].size()<obsTraj[0].size()){
			std::cout << "[ERROR] Hint: Check bounds of alpha when calling Predictor::predictTimeMod or check length of obsTraj in Predictor::inferProMP" << std::endl;
			return -1;
		}
		idx++;
		int size;
		#pragma omp parallel for
		for (int n=0; n<obsTraj.size(); n++){
			if(obsTraj[n].size()<meanTraj[n].size())
				size = obsTraj[n].size();
			else 
				size = meanTraj[n].size();
			for (int k=0; k<size; k++){
				dist += abs((obsTraj[n])(k) - (meanTraj[n])(k));
			}
		}
		// dist += abs(obsTraj1(0) - meanTraj1(0));
		distance.push_back(dist);
		//std::cout << dist << std::endl;
		dist=0;
	}
	int infrd_id = std::min_element(distance.begin(),distance.end()) - distance.begin();
	if (distance[infrd_id]>infr_threshold[infrd_id])
		return -1;
	else
		return infrd_id;
}


ProMP Predictor::predictFuture(const Eigen::VectorXd &obsTraj, ProMP infrdPromp, const double &infrdTimeMod){
	// get ProMP data
	Eigen::MatrixXd covW = infrdPromp.getCovariance();
	Eigen::VectorXd meanW = infrdPromp.getWeights();
	int numBf = meanW.size();
	// get duration of observation
	int dt_obs = obsTraj.size();
	// create data noise matrix
	Eigen::MatrixXd expNoise(dt_obs,dt_obs);
	expNoise = Eigen::MatrixXd::Identity(dt_obs,dt_obs)*expNoise_;
	// Set inferred time modulation and update phase of inferred promp
	Eigen::VectorXd meanTraj = infrdPromp.generateTrajectory(infrdTimeMod);
	infrdPromp.setTimeMod(infrdTimeMod);
	infrdPromp.computePhase();
	// get phase vector from inferred ProMP
	Eigen::VectorXd phase = infrdPromp.getPhase();
	// get observed basis functions vector
	Eigen::MatrixXd phi = infrdPromp.generateBasisFunction(phase);
	Eigen::MatrixXd phi_obs = phi.block(0,0,numBf,dt_obs);
	//Estimation terms
	Eigen::MatrixXd K(numBf,dt_obs);
	Eigen::VectorXd est_meanW(numBf);
    Eigen::MatrixXd est_covW(numBf,numBf);
	Eigen::MatrixXd phi_obs_T = phi_obs.transpose();
	auto eye = MatrixXd::Identity(dt_obs,dt_obs);

	//Compute estimation terms
	Eigen::MatrixXd phi_o2cov = expNoise+phi_obs_T*covW*phi_obs;
	// phi_o2cov is often singular. Add ridge factor. Consider that we are dealing with large value matrices
	double ridge_factor = 0.1;
	// divide and remultply by a factor to avoid numeric explosion
	K = (covW*phi_obs*0.00000001)*(phi_o2cov+ridge_factor*eye).inverse();
	K*=100000000;
	est_meanW = meanW + K*(obsTraj - phi_obs_T*meanW);
	est_covW = covW - K*(phi_obs_T*covW);

	ProMP predPromp(est_meanW,est_covW,numBf,infrdPromp.getStdBf(),infrdPromp.getNoSamples(),infrdTimeMod,phase,phi);
	return predPromp;
}