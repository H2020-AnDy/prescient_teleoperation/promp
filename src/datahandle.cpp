#include "dataHandle.hpp"

DataHandle::DataHandle(int index,std::vector<std::string> fileList):index_(index),fileList_(fileList){
	//list of trajectory lengths
	std::vector<int> trjLens;

	for(int i=0;i<fileList_.size();++i){
		CSVReader reader(fileList_[i]);
		Eigen::VectorXd data = reader.get1Ddata(index_,reader.getData());
		trjLens.push_back(data.size());
		trajList_.push_back(data);
	}

	int minLen = *std::min_element(trjLens.begin(), trjLens.end());
	reqTrajLen_=minLen;
	
	// modulates every trajectory in trajList_ to slowest trajectory
	double delta;
	Eigen::MatrixXd data1(fileList_.size(),reqTrajLen_);
	for(int i=0;i<trajList_.size();++i){			
		delta = (trajList_[i].innerSize() - 1)*1.0/(reqTrajLen_-1);
		deltaList_.push_back(delta);
		for(int j=0;j<reqTrajLen_;++j){
			data1(i,j)=trajList_[i]((int)(j*delta));
		}
	}
	data_= data1;
}