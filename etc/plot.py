import numpy as np
import sys
import matplotlib.pyplot as plt

#----------------SIMPLE PROMP------------------------------------------
if len(sys.argv) > 1:
	data = np.genfromtxt(sys.argv[1],delimiter=',', dtype = float)
else:
	data = np.genfromtxt('../build/my_promp.csv',delimiter=',', dtype = float)
data1 = np.genfromtxt('../build/my_promp1.csv',delimiter=',', dtype = float)

democolor = 'black'

demo1 = [row[0] for row in data]
demo2 = [row[1] for row in data]
demo3 = [row[2] for row in data]
demo4 = [row[3] for row in data]
demo5 = [row[4] for row in data]
demo6 = [row[5] for row in data]
demo7 = [row[6] for row in data]
demo8 = [row[7] for row in data]
demo9 = [row[8] for row in data]
demo10 = [row[9] for row in data]
mean_traj = np.array([row[10] for row in data])
std_traj = np.array([row[11] for row in data])

mean_traj_std = np.array([row[0] for row in data1])
std_traj_std = np.array([row[1] for row in data1])

fig = plt.figure()


plt.plot(demo1,democolor)
plt.plot(demo2,democolor)
plt.plot(demo3,democolor)
plt.plot(demo4,democolor)
plt.plot(demo5,democolor)
plt.plot(demo6,democolor)
plt.plot(demo7,democolor)
plt.plot(demo8,democolor)
plt.plot(demo9,democolor)
plt.plot(demo10,democolor)
plt.plot(mean_traj,'g',linewidth=4.0)

plt.fill_between(np.linspace(0, mean_traj.size-1, num=mean_traj.size), mean_traj - std_traj, mean_traj + std_traj, color='green', alpha=0.2)

#plt.plot(mean_traj_std,'r',linewidth=4.0)
#plt.fill_between(np.linspace(0, mean_traj_std.size-1, num=mean_traj_std.size), mean_traj_std - std_traj_std, mean_traj_std + std_traj_std, color='red', alpha=0.2)

#---------------------PCA--------------------------------------------
#dataobs = np.genfromtxt('../build/obs.csv',delimiter=',', dtype = float)
#data1 = np.genfromtxt('../build/1_infrd.csv',delimiter=',', dtype = float)
#data2 = np.genfromtxt('../build/2_infrd.csv',delimiter=',', dtype = float)
#data3 = np.genfromtxt('../build/3_infrd.csv',delimiter=',', dtype = float)
#dataobs_ = np.genfromtxt('../build/obs_.csv',delimiter=',', dtype = float)
#data1_ = np.genfromtxt('../build/1_infrd_.csv',delimiter=',', dtype = float)
#data2_ = np.genfromtxt('../build/2_infrd_.csv',delimiter=',', dtype = float)
#data3_ = np.genfromtxt('../build/3_infrd_.csv',delimiter=',', dtype = float)
#
#
#obs_traj = np.array(dataobs)
#mean_traj_std = np.array([row[0] for row in data1])
#std_traj_std = np.array([row[1] for row in data1])
#mean_traj_std2 = np.array([row[0] for row in data2])
#std_traj_std2 = np.array([row[1] for row in data2])
#mean_traj_std3 = np.array([row[0] for row in data3])
#std_traj_std3 = np.array([row[1] for row in data3])
#
#obs_traj_ = np.array(dataobs_)
#mean_traj_std_ = np.array([row[0] for row in data1_])
#std_traj_std_ = np.array([row[1] for row in data1_])
#mean_traj_std2_ = np.array([row[0] for row in data2_])
#std_traj_std2_ = np.array([row[1] for row in data2_])
#mean_traj_std3_ = np.array([row[0] for row in data3_])
#std_traj_std3_ = np.array([row[1] for row in data3_])
##
##
##
#fig = plt.figure()
#plt.plot(obs_traj,'b',linewidth=4.0)
#
#plt.plot(mean_traj_std,'r',linewidth=4.0)
#plt.fill_between(np.linspace(0, mean_traj_std.size-1, num=mean_traj_std.size), mean_traj_std - std_traj_std, mean_traj_std + std_traj_std, color='red', alpha=0.2)
#
#plt.plot(mean_traj_std2,'g',linewidth=4.0)
#plt.fill_between(np.linspace(0, mean_traj_std2.size-1, num=mean_traj_std2.size), mean_traj_std2 - std_traj_std2, mean_traj_std2 + std_traj_std2, color='green', alpha=0.2)
##
#plt.plot(mean_traj_std3,'black',linewidth=4.0)
#plt.fill_between(np.linspace(0, mean_traj_std3.size-1, num=mean_traj_std3.size), mean_traj_std3 - std_traj_std3, mean_traj_std3 + std_traj_std3, color='black', alpha=0.2)
#
#plt.show()
#
#fig = plt.figure()
#plt.plot(obs_traj_,'b',linewidth=4.0)
#
#plt.plot(mean_traj_std_,'r',linewidth=4.0)
#plt.fill_between(np.linspace(0, mean_traj_std_.size-1, num=mean_traj_std_.size), mean_traj_std_ - std_traj_std_, mean_traj_std_ + std_traj_std_, color='red', alpha=0.2)
#
#plt.plot(mean_traj_std2_,'g',linewidth=4.0)
#plt.fill_between(np.linspace(0, mean_traj_std2_.size-1, num=mean_traj_std2_.size), mean_traj_std2_ - std_traj_std2_, mean_traj_std2_ + std_traj_std2_, color='green', alpha=0.2)
##
#plt.plot(mean_traj_std3_,'black',linewidth=4.0)
#plt.fill_between(np.linspace(0, mean_traj_std3_.size-1, num=mean_traj_std3_.size), mean_traj_std3_ - std_traj_std3_, mean_traj_std3_ + std_traj_std3_, color='black', alpha=0.2)
#
#
#
#plt.show()

#---------------------FUTURE--------------------------------------------
#dataobs = np.genfromtxt('../build/obs.csv',delimiter=',', dtype = float)
#data1 = np.genfromtxt('../build/learned.csv',delimiter=',', dtype = float)
#data2 = np.genfromtxt('../build/future.csv',delimiter=',', dtype = float)
#data3 = np.genfromtxt('../build/condition.csv',delimiter=',', dtype = float)
#
#
#mean_traj_std = np.array([row[0] for row in data1])
#std_traj_std = np.array([row[1] for row in data1])
#obs_traj = np.array(dataobs)
#mean_traj_std2 = np.array([row[0] for row in data2])
#std_traj_std2 = np.array([row[1] for row in data2])
#mean_traj_std3 = np.array([row[0] for row in data3])
#std_traj_std3 = np.array([row[1] for row in data3])
##
##
##
#fig = plt.figure()
#plt.plot(obs_traj,'b',linewidth=4.0)
#
#plt.plot(mean_traj_std,'r',linewidth=4.0)
#plt.fill_between(np.linspace(0, mean_traj_std.size-1, num=mean_traj_std.size), mean_traj_std - std_traj_std, mean_traj_std + std_traj_std, color='red', alpha=0.2)
#
#plt.plot(mean_traj_std2,'g',linewidth=4.0)
#plt.fill_between(np.linspace(0, mean_traj_std2.size-1, num=mean_traj_std2.size), mean_traj_std2 - std_traj_std2, mean_traj_std2 + std_traj_std2, color='green', alpha=0.2)
##
#plt.plot(mean_traj_std3,'black',linewidth=4.0)
#plt.fill_between(np.linspace(0, mean_traj_std3.size-1, num=mean_traj_std3.size), mean_traj_std3 - std_traj_std3, mean_traj_std3 + std_traj_std3, color='black', alpha=0.2)
#
#
#
#plt.show()