# ProMP C++

## What is this 

This is a library in C++ that implements Probabilistic Motion Primitives algorithms ([Original Paper](https://link.springer.com/article/10.1007/s10514-017-9648-7)).

Initially, it was highly inspired in the following python implementations:
* [promps_python](https://github.com/mjm522/promps_python)
* [promplib](https://github.com/baxter-flowers/promplib)

## Requirements

* C++ 11
* CMake 2.8.12
* Eigen 3.2.92
* Doxygen 1.8.11
* OpenMP

(Other versions of CMake, Eigen, Doxygen will probably work, but those are the tested ones so far)

## Optional Requirements (mandatory for features that use PCA)

* libpca-1.3.3
* armadillo-9.850.1
* OpenBLAS-0.3.6
* SuperLU_5.2.1


## Requirements Installation

### Mandatory Requirements

- To install OpenMP:
```
sudo apt-get install libomp-dev
```

### PCA and its Dependencies (Optional)

 *libpca* is built upon *armadillo* which requires *OpenBLAS* and *SuperLU*.

You should install cmake, gfortran, LAPACK, BLAS, ATLAS and ARPACK:
```
sudo apt-get install cmake cmake-gui gfortran 
sudo apt-get install liblapack-dev libblas-dev libatlas-base-dev libarpack2-dev libarpack++2-dev
```
Note that because we're going to use OpenBLAS, it is not necessary to install BLAS or ATLAS.

- To install OpenBlas:
```
git clone https://github.com/xianyi/OpenBLAS.git
cd OpenBLAS
make
sudo make install
```

- To install SuperLU:
```
git clone https://github.com/xiaoyeli/superlu.git
cd SuperLU
mkdir build && cd build
cmake ..
make
sudo make install
```
 
- To install armadillo:
```
git clone https://gitlab.com/conradsnicta/armadillo-code.git
cd armadillo
cmake .
make
sudo make install
```

- To install libpca:
```
git clone https://github.com/spinlockirqsave/libpca-1.2.11.git
cd libpca
./configure
make
sudo make install
```


## Installation

```
git clone https://gitlab.inria.fr/H2020-AnDy/promp.git
cd promp
mkdir build
cd build
ccmake ..
```

If you wish to build doxygen documentation set BUILD_DOC to ON before gererating makelists.

If you wish to use pca set BUILD_PCA to ON before gererating makelists.

Then, proceed to make and install:

```
make
sudo make install
```
If while executing your compiled targets you get an error like the following:
> cannot open shared object file: No such file or directory

Please try running ldconfig before running again:
```
sudo ldconfig
```

For the moment, we have only tested this library on Ubuntu 16.04.

## Usage

You can refer to the source code of some examples in the 'example' folder to understand the usage of the library. Their binaries are automatically generated after the installation.


In the folder 'etc/demos/' you can find many recordings of human whole-body postures (i.csv i=1,2,...) and/or body segments poses  (pi.csv i=1,2,...) for several motions. With these you can train and play with your own ProMPs. Have fun!


## Acknowledgments

The development of this software is partially supported by [the European Project H2020 An.Dy](http://andy-project.eu/).


