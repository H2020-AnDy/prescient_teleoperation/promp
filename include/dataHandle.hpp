/** 
\author Vishnu Radhakrishnan, Luigi Penco
*/

#ifndef DATA_HANDLE
#define DATA_HANDLE
#include <vector>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <iostream>
#include <string>
#include "csvReader.hpp"
/**
 * @brief      Class for handling data from multiple demonstrations.
* Data(all trajectories) from each demonstration should be stored in an individual .csv file 
* Each column within .csv file represents a trajectory.
* All .csv files must have trajectories(columns) in the same sequence (column-wise)
 */
class DataHandle{

public:
	/** 
	*   @\brief				 Normalize the trajectory demonstrations to the same time-step length
	*	@\param	reqTrajLen   required length of trajectory
	*	@\param	index    	 column of required trajectory in each csv file
	*	@\param	fileList  	 vector of .csv file names	
	*/
	DataHandle(int index,std::vector<std::string> fileList);
	/** 
	 * @\brief	get required data
	*/
	Eigen::MatrixXd getData(){
		return data_;
	}
	/** 
	 * @\brief	get trajectory list
	*/
	std::vector<Eigen::VectorXd> getTrajList(void){
		return trajList_;
	}
	/** 
	 * @\brief	get required list of time modulations of each trajectory w.r.t. the slowest
	 * @return  deltaList_   list of time modulations of each trajectory
	*/
	std::vector<double> getDeltaList(){
		return deltaList_;
	}

private:
	//! approximate length
	int reqTrajLen_;
	//! column number of desired trajectory in each csv file.
	int index_;
	//! file names with trajectories for each demonstration.
	std::vector<std::string> fileList_;
	//! std::vector of Eigen::VectorXd of trajectories
	std::vector<Eigen::VectorXd> trajList_;
	//! Eigen::Matrix of trajectories
	Eigen::MatrixXd data_;
	//! std::vector of the time modulation of each trajectory w.r.t. the slowest (i.e. the ProMP's \f$ \alpha_ \f$)
	std::vector<double> deltaList_;
};
#endif