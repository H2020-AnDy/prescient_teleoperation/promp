/** 
\author Vishnu Radhakrishnan
\author Waldez Gomes
*/

#ifndef PROMP
#define PROMP

#include <iostream>
#include <cmath>
#include <math.h>
#include <vector>
#include <iterator>
#include <string>
#include <random>
#include <memory>
#include "headerUtils.hpp"
#include "dataHandle.hpp"
#include <omp.h> 
#include <stdio.h> 

#define OBS_ERROR 0.01

using namespace Eigen;

/**
 * @brief      Class that implements functions for using uni-dimensional Probabilistic Motion Primitives like in the
 * <a href="https://link.springer.com/article/10.1007/s10514-017-9648-7">original paper</a>. For
 * multiple dimensions, multiple ProMP objects must be used.
 */
class ProMP{
public:
	/** \brief constructor: The constructor will parameterize a phase vector, and compute the basis function matrix 
	 * \f$ \Psi \f$ for all phase steps. Then, for each demonstration within the matrix \p data, it will estimate a
	 *  vector of basis functions' weights \f$ w_i \f$.
	 * Lastly, it fits a gaussian over all the weight vectors to obtain \f$ \mu_w \f$, and \f$ \Sigma_w \f$
	 * 
	*	\param data 	(numDemos_,trajLength_) data matrix
	*	\param numBf 	 number of basis functions
	*	\param stdBf	 standard deviation 
	*/
	ProMP(Eigen::MatrixXd& data,int numBf, double stdBf);
	/**
	 * @brief      This is an alternate constructor that uses a filelist and a column index \p  dataColumn to acquire
	 * the demonstration data from inside the constructor
	 *
	 * @param[in]  fileList    The file list
	 * @param[in]  dataColumn  The data column index
	 * @param[in]  numBf       The number of basis function
	 * @param[in]  stdBf       The standard deviation of each basis function
	 */
	ProMP(std::vector<std::string> fileList, int dataColumn, int numBf, double stdBf);
	/**
	 * @brief      This is an alternate constructor that uses prelearned weights and covariance
	 *
	 * @param[in]  w           The mean of weights' distribution
	 * @param[in]  covW        The co-variance of weights' distribution
	 * @param[in]  numBf       The number of basis function
	 * @param[in]  stdBf       The standard deviation of each basis function
	 * @param[in]  n_sample    The number of samples required for the trajectory
	 * @param[in]  m_alpha     Mean of time modulation in the demo trajectories used to train this ProMP
	 */
	ProMP(Eigen::VectorXd w, Eigen::MatrixXd covW, int numBf, double stdBf, int n_sample, double m_alpha);

	/**
	 * @brief      This is an alternate constructor that uses prelearned weights, covariance and number of samples in the trajectory
	 *
	 * @param[in]  w           The mean of weights' distribution
	 * @param[in]  covW        The co-variance of weights' distribution
	 * @param[in]  numBf       The number of basis function
	 * @param[in]  stdBf       The standard deviation of each basis function
	 * @param[in]  n_sample    The number of samples required for the trajectory
	 */
	ProMP(Eigen::VectorXd w, Eigen::MatrixXd covW, int numBf, double stdBf, int n_sample);

	/**
	 * @brief      This is an alternate constructor for fast computations that uses all the prelearned parameters
	 *
	 * @param[in]  w           The mean of weights' distribution
	 * @param[in]  covW        The co-variance of weights' distribution
	 * @param[in]  numBf       The number of basis function
	 * @param[in]  stdBf       The standard deviation of each basis function
	 * @param[in]  n_sample    The number of samples required for the trajectory
	 * @param[in]  alpha       The time modulation
	 * @param[in]  phase       The phase vector
	 * @param[in]  phi    	   The rbf matrix
	 */
	ProMP(Eigen::VectorXd w, Eigen::MatrixXd covW, int numBf, double stdBf, int n_sample, double alpha, Eigen::VectorXd phase, Eigen::MatrixXd phi);

	ProMP operator- () const;

	/**
	 * @brief      Gets the basis function matrix for the current trained ProMP.
	 *
	 * @return     The basis function \f$ \Psi \f$.
	 */
	Eigen::MatrixXd getBasisFunction();

	/** \brief	generates basis functions
	*	\param phase
	*	\return matrix of basis functions
	*/
	Eigen::MatrixXd generateBasisFunction(const Eigen::VectorXd &phase);

	/** \brief	maps time vector into a phase vector.
	*/
	void computePhase();

	/**
	 * @brief      Gets the phase vector.
	 *
	 * @return     The phase vector.
	 */
	Eigen::VectorXd getPhase();

	/**
	 * @brief      Gets the vector of the mean of all weights \f$ \mu_w \f$.
	 *
	 * @return     The weights.
	 */
	Eigen::VectorXd getWeights();

	/**
	 * @brief      Sets the weights. Beware that the weights must have been trained somehow outside of the class (in an optimization
	 * framework for instance).
	 *
	 * @param[in]  w     { parameter_description }
	 */
	void setWeights(const Eigen::VectorXd& w);

	/**
	 * @brief      Gets the co-variance matrix \f$ \Sigma_w \f$.
	 *
	 * @return     The co-variance matrix.
	 */
	Eigen::MatrixXd getCovariance();

	/**
	 * @brief      Sets the covariance matrix.
	 *
	 * @param[in]   cov  The co-variance matrix
	 */
	void setCovariance(const Eigen::MatrixXd& cov);

	/**
	 * @brief      Sets the number of samples \f$ \s_ \f$ for the trajectories.
	 *
	 * @param[in]  n_sample    #points in the entire trajectory
	 */
	void setNoSamples(int n_sample);

	/**
	 * @brief      Gets the number of samples for the trajectory \f$ \s_ \f$.
	 *
	 * @return     The number of samples for the trajectory
	 */
	int getNoSamples();

	/**
	 * @brief      Gets the trajectory length.
	 *
	 * @return     The trajectory length
	 */
	int getTrajLength();

	/**
	 * @brief      Gets the std deviation \f$ \stdBf \f$ of the ProMP
	 * @return     The std deviation.
	 */
	double getStdBf();

	/**
	 * @brief      Sets the time modulation of the trajectory.
	 *
	 * @param[in]  timeMod    time modulation
	 */
	void setTimeMod(double timeMod);

	/**
	 * @brief      Gets the average time modulation in the demonstrations used to train the ProMP
	 * @return     m_alpha_ The avergae demo time modulation
	 */
	double getMeanDemoTimeMod();

	/**
	 * @brief      Generates MEAN trajectory based on current weights distribution and rbf
	 * 
	 * @return     Mean trajectory
	 */
	Eigen::VectorXd generateTrajectory();

	/**
	 * @brief      Generates MEAN trajectory based on current weights distribution and
	 * a required number of steps
	 *
	 * @param[in]  reqNumberSteps  The requested number of steps for trajectory
	 *
	 * @return     Mean trajectory
	 */
	Eigen::VectorXd generateTrajectory(int reqNumberSteps);

	/**
	 * @brief      Generates MEAN trajectory based on current weights distributions
	 * and a required phase speed \f$ \dot{z}_t \f$.
	 *
	 * @param[in]  reqPhaseSpeed  The request phase speed. <b> To play trajectory at orignal speed set 
	 * \f$ \dot{z}_t = 1.0 \f$ </b>.
	 *
	 * @return     { description_of_the_return_value }
	 */
	Eigen::VectorXd generateTrajectory(double reqPhaseSpeed);
	
	/**
	 * @brief      Generates trajectory with a different vector weights than the trained/conditioned \f$ \mu_w \f$
	 *
	 * @param[in]  weights          The weights
	 * @param[in]  requiredTrajLen  The required traj length (number of time steps)
	 *
	 * @return     { description_of_the_return_value }
	 */
	Eigen::VectorXd generateTrajectory(Eigen::VectorXd weights,int requiredTrajLen);

	Eigen::VectorXd	generateTrajectory(Eigen::VectorXd weights,double reqPhaseSpeed);
	
	/** \brief	\deprecated generates average trajectory
	*	\return trajectory
	*/
	Eigen::VectorXd generateAvgTrajectory();
	
	/** \brief	set desired goal/end point for trajectory
	*	\param 	goal 	desired value at end
	*	\param	std 	desired standard deviation. <b> typically around \f$ 10^{-6}\f$ for accuracy </b>
	*	\return 
	*/
	void setGoal(double goal,double std);
	
	/** \brief	set desired start/initial point for trajectory
	*	\param 	start 	desired value at start
	*	\param	std   	desired standard deviation.<b> typically around \f$ 10^{-6}\f$ for accuracy </b>
	*	\return
	*/
	void setStart(double start,double std);
	
	/** \brief	set via point for trajectory
	*	\param 	t 			time at which via point is to be added (between 0 and LAST TIME STEP)
	*	\param	viaPoint 	desired value at via point
	*	\param	std   		desired standard deviation.<b> typically around \f$ 10^{-6}\f$ for accuracy </b>
	*	\return 
	 *	\todo Use phase \f$ z_t\f$ instead of time step t.
	*/
	void addViaPoint(double t, double viaPoint, double std);
	
	/** \brief	get average starting value of demo trajectories
	*	\return average starting value
	*/
	double getAvgStart();
	
	/** \brief	get average ending value of demo trajectories
	*	\return average ending value
	*/
	double getAvgEnd();

	/**
	 * @brief      Generates standard deviation vector with the standard deviation for every time step
	 *
	 * @return     Standard Deviation Vector \f$ DIAG( \Sigma ) \f$
	 */
	Eigen::VectorXd genTrajStdDev();
	/**
	 * @brief      Generates standard deviation vector with the standard deviation for every time step, with a certain number of time steps
	 *
	 * @param[in]  reqNumberSteps  The requested number steps
	 *
	 * @return     Standard Deviation Vector \f$ DIAG( \Sigma ) \f$
	 */
	Eigen::VectorXd genTrajStdDev(int reqNumberSteps);


	/**
	 * @brief      Conditions all via points registered in 'viaPoints_'.
	 * It updates  \a meanW_ and covW_ 
	 */
	void conditionViaPoints(void);

	Eigen::VectorXd getUpperWeights(double K);

	Eigen::VectorXd getLowerWeights(double K);

	/**
	 * @brief      Sets the identifier.
	 *
	 * @param[in]  id    The new value
	 */
	void setId(std::string id);

	/**
	 * @brief      Gets the identifier.
	 *
	 * @return     The identifier.
	 */
	std::string getId(void);

	/**
	 * @brief      Overloads << operator in order to print the weights of a given promp
	 *
	 * @param      out   The out
	 * @param[in]  mp    THIS promp
	 *
	 */
	friend ostream & operator << (ostream &out, ProMP &mp);

	/**
	 * @brief Get the Mean Weights From List object
	 * 
	 * @param mpList 
	 * @return std::vector<double> 
	 */
	static std::vector<double> getMeanWeightsFromList(std::vector<ProMP> mpList);

	/**
	 * @brief Save generated
	 * 
	 * @param filename 
	 * @return true 
	 * @return false 
	 */
	bool saveTrajToFile(std::string filename);


	
private:

	/**
	 * @brief      train using demonstration trajectories to obtain average weights
	 *
	 * @param      dataBase  matrix of demonstration trajectories
	 */
	void train(Eigen::MatrixXd& dataBase);

	/** \brief	utility function. new vector with values being a progressive cumulative sum of given vector elements
	*/
	Eigen::VectorXd cumSum(Eigen::VectorXd vect);

	/** \brief 				get phase for a specific point in the trajectory
	*	\param				time step of which phase is to be found
	*	\return 			phase
	*/
	Eigen::VectorXd getPhaseFromTime(int timeSteps);

	/** \brief	maps time vector into a phase vector.
	*/
	void setPhase(Eigen::VectorXd z);

	/**
	 * @brief      Calculates the phase for a required number of steps
	 *
	 * @param[in]  reqNumberSteps  The required number of steps
	 */
	void computePhaseForSteps(int reqNumberSteps);

	/**
	 * @brief      Gets the time modulation.
	 *
	 * @return     The time modulation
	 */
	double getTimeMod();

	/** \brief	linear interpolator
	*	\param start - start value
	*	\param end - end value
	*	\param number - number of divisions
	*	\return vector of equidistant values
	*/
	Eigen::VectorXd linspace(double start, double end, int number);
	/// utility function
	Eigen::MatrixXd rbfGaussian(Eigen::MatrixXd& phaseDiff);

	void computeDelta(double K);

	//! ProMp identifier
	std::string id_;
	//! number of basis functions
	int numBf_;
	//! standard deviation of basis functions	
	double stdBf_;
	//! Basis Functions' Centers
	Eigen::VectorXd c_;
	//! Basis Function Matrix
	Eigen::MatrixXd phi_;
	Eigen::MatrixXd phiInv_;
	Eigen::MatrixXd phaseDiff_;
	double phaseEnd_;
	//! number of demonstrations
	int numDemos_;
	//! number of points in the entire trajectory
	int trajLength_;
	//! average start value of all demo trajectories
	double start_;
	//! average end value of all demo trajectories
	double end_;
	
	//! average trajectory directly from demonstrations
	Eigen::VectorXd means_;
	//! average time modulation in the demonstrations used to train the ProMP
	double m_alpha_;

	//! @brief std::vector of Eigen::Vector3d for storing information for adding via points in the trajectory
	std::vector<Eigen::Vector3d>viaPoints_;
	//! phase vector
	Eigen::VectorXd phase_;
	//! time modulation
	double alpha_=1.0;
	//! number of samples used to rescale all the trajectories to same duration
	int s_;
	//! Mean of weights' distribution
	Eigen::VectorXd meanW_;
	Eigen::VectorXd sampleW_;
	//! Co-variance of weights' distribution
	Eigen::MatrixXd covW_;

	Eigen::VectorXd delta_wy_;
};

/**
 * Shared Pointer Declaration For ProMP Class
 */
typedef std::shared_ptr<ProMP> ProMpPtr;

#endif