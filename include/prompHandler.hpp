/**
* Copyright: (C) 2019 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*/
#ifndef PROMPHANDLER
#define PROMPHANDLER

#include "promp.hpp"
#include <algorithm>
#include <boost/filesystem.hpp>
#ifdef USE_PCA
#include <pca.h>
#endif



/**
 * @brief      Class that implements functions for loading and saving and encoding proMPs
 */
class PrompHandler{
public:
	/** \brief				Class constructor
	*/
	PrompHandler();

	/** \brief				Class constructor
	*	\param	promp_demos   number of demos used for the generation of the ProMP
	*/
	PrompHandler(int promp_demos);

	#ifdef USE_PCA
	/** \brief				Class constructor
	*	\param	promp_demos   number of demos used for the generation of the ProMP
	*	\param	data_dim   	  dimension of the original data
	*	\param	encoded_dim   dimension of the encoded data
	*/
	PrompHandler(int promp_demos, int data_dim, int encoded_dim);

	/** \brief				Class constructor
	*	\param	promp_demos   number of demos used for the generation of the ProMP
	*	\param	data_dim   	  dimension of the original data
	*	\param	encoded_dim   dimension of the encoded data
	*/
	void init(int promp_demos, int data_dim, int encoded_dim);

	/** \brief				Load dataset to train the PCA encoding
	*	\param	file_path   path of csv file containing the data
	*	\return dataset
	*/
	Eigen::MatrixXd loadPCADataset(string file_path);
	
	/** \brief				Encode the data w/ PCA
	*	\param	qt_train    training data
	*/
	void findPCAEncoding(const Eigen::MatrixXd &qt_train);

	/** \brief				Load PCA encoding
	*/
	void loadPCA();

	/** \brief				Get the mean value of the data in the PCA endoding dataset
	*	\return mean value of dataset
	*/
	Eigen::VectorXd getMeanPCADataset();

	/** \brief				Encode the data w/ PCA
	*	\param	qt_train    training data
	*	\return encoded data matrix
	*/
	Eigen::MatrixXd encodew_PCA(const MatrixXd &m_data);

	/** \brief				Encode the ProMP data w/ PCA and save the result in a csv file
	*	\param	motion    name of the folder of the motion data
	*/
	void encode_train(string motion, string jbs);

	/** \brief				Learn the ProMPs from encoded data in the folder motion
	*	\param	motion    name of the folders of the motion data
	*	\param  jbs		 j r or p (joint, retargeted joint or body segment)
	*	\param  n_rbf 	 number of basis functions
	*	\param  stdDev	 standard deviation 
	*/
	void learnEncodedPromp(string motion, string jbs, const int &n_rbf, const double &stdDev);
	#endif
	/** \brief				Learn the ProMPs from original data in the folder motion
	*	\param	motion    name of the folder of the motion data
	*	\param  jbs		 j r or p (joint, retargeted joint or body segment)
	*	\param	jointID   vector with ids of the joints/body segments of interest
	*	\param  n_rbf 	 number of basis functions
	*	\param  stdDev	 standard deviation 
	*/
	void learnPromp(string motion, string jbs, const VectorXi &ID, const int &n_rbf, const double &stdDev);

	/** \brief				Learn a ProMP from original data in the folder motion
	*	\param	motion    name of the folder of the motion data
	*	\param  jbs		 j r or p (joint, retargeted joint or body segment)
	*	\param	ID   int id of the joint/body segment of interest
	*	\param  n_rbf 	 number of basis functions
	*	\param  stdDev	 standard deviation 
	*/
	void learnPromp(string motion, string jbs, int ID, const int &n_rbf, const double &stdDev);

	/** \brief				 	Get a vector of n 1D ProMPs for a specific motion
	*	\param	motion      name of the folder of the motion data
	*	\param  jbs		 	j r or p (joint, retargeted joint or body segment)
	*	\param	encoded   	to get the ProMp from encoded or orginal data
	*	\return list of n 1D ProMPs
	*/
	std::vector<ProMP> getPromp(string motion, string jbs, const bool &encoded);

	/** \brief				 	Get a 1D ProMP for a specific motion
	*	\param	motion      name of the folder of the motion data
	*	\param  jbs			 j r or p (joint, retargeted joint or body segment)
	*	\param	id   	    id number of the promp file
	*	\param	encoded   	to get the ProMp from encoded or orginal data
	*	\return 1D ProMP
	*/
	ProMP getPromp(string motion, string jbs, int id, const bool &encoded);

	/** \brief				 	Create retargeted robot trajectories csv file
	*	\param	motion      	name of the folder of the motion data
	*/
	void createRetargetedQTraj(string motion);

	/** \brief				 	Retarget human joint trajectories into robot trajectories
	*	\param	data      		data matrix (trajectory length, #joints human)
	*	\return retargeted data matrix (trajectory length, #joints robot)
	*/
	Eigen::MatrixXd retargetQTraj(Eigen::MatrixXd data);

	/** \brief				 	Compute the classification threshold for the learned ProMP
	*	\param	motion      name of the folder of the motion data
	*	\param  jp		    j r or p (joint, retargeted joint or body segment)
	*	\param	id   	    id number of the promp file
	*	\param	encoded   	to get the ProMp from encoded or orginal data
	*	\param	der   		to compute the threshold for a classification based on the difference the ProMPs mean trajectories or on the difference of their derivates
	*	\param	obs_duration   	duration of the observation
	*	\return classification threshold for a given motion
	*/
	double computeMotionThreshold(string motion, string jbs, int id, const bool &encoded, int obs_duration);

	/** \brief				 	Save in a file the classification threshold for the learned ProMPs
	*	\param	threshold_vec       vector of the classification thresholds of the considered motions
	*/
	void saveMotionsThreshold(vector<double> threshold_vec);

	/** \brief				 	Get the classification threshold for the learned ProMPs from file
	*	\return classification threshold vector for each given motion
	*/
	Eigen::VectorXd getMotionsThreshold();
private:
    //! #repetitions of a motion used to learn the promps 
    int promp_demos_;
    //! original data dimension
    int data_dim_;
    //! #PC for the encoding
    int encoded_dim_;
    #ifdef USE_PCA
    //! PCA object
    stats::pca pca;
    //! Row Feature vector. Principal Components (data_dim_ vectors) as rows (Most significant PC at the top) (#PC=encoded_dim)
    std::vector<std::vector<double>> FeatureM_;
    Eigen::MatrixXd FeatureM;
    //! Mean vector of the data in the training dataset for PCA
    VectorXd mean_;
    #endif
    //! Mean of the time modulation in the demo trajectories used for training the ProMP
    double mean_alpha_;
    //file for saving data
    ofstream myfile;
    double id_threshold_;

};
#endif
