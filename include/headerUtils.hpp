/** 
\author Waldez Gomes
*/

#ifndef HEADER_UTILS_H
#define HEADER_UTILS_H

#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Core>

using namespace Eigen;
using namespace std;

#define SIZE(V) cout<< #V <<" has: "<<V.rows()<<" rows, and "<<V.cols()<<" columns"<<endl;


#endif //