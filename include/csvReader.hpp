/** 
\author Vishnu Radhakrishnan
*/

#ifndef CSV_READER
#define CSV_READER
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <fstream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <iostream>
#include <string>

/**
 * @brief      Class handling data from .csv files.
 */
class CSVReader{
private:
	std::string fileName_;
	std::string delimeter_; 
	std::vector<std::vector<std::string> > dataList_;
	std::vector<std::vector<double>> list;
	bool skipHeader_;

public:
	CSVReader(std::string filename, std::string delm = ",", bool skipHeader = false);

	Eigen::MatrixXd getData();

	Eigen::VectorXd get1Ddata(int index,Eigen::MatrixXd data);
};

#endif
