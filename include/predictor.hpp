/**
* Copyright: (C) 2019 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*/
#ifndef PREDICTOR
#define PREDICTOR

#include "promp.hpp"
#include <algorithm>
#include <type_traits>
#include <Eigen/LU> 
#include <Eigen/QR> 
#include <omp.h> 
#include <stdio.h> 

/**
 * @brief      Class that implements functions for the prediction of the evolution of a certain movement
 */
class Predictor{
public:
	/** \brief				Class constructor
	*	\param	dataNoise   expected noise in the observation
	*/
	Predictor(double dataNoise=0.0001);

	/** \brief				 Given an observed (portion of) trajectory, predict the mostly likely time modulation for a given ProMPs
	*	\param	obsTraj      observed (portion of) trajectory
	*	\param	promp        candidate ProMP
	*	\param	lb      	 lower bound for \f$ alpha\f$
	*	\param	ub    	 	 upper bound for \f$ alpha\f$
	*	\param	step    	 distance among each candidate \f$ alpha\f$ values
	*	\return predicted time modulations \f$ alpha\f$ for the ProMP
	*/
	double predictTimeMod(const Eigen::VectorXd &obsTraj, ProMP promp, double lb, double ub, double step);

	/** \brief				 Given an observed (portion of) trajectory, predict the mostly likely time modulation for a given ProMPs
	*	\param	obsTraj      vector of observed (portions of) trajectories
	*	\param	promp        candidate ProMPs (vector of 1D ProMPs)
	*	\param	lb      	 lower bound for \f$ alpha\f$
	*	\param	ub    	 	 upper bound for \f$ alpha\f$
	*	\param	step    	 distance among each candidate \f$ alpha\f$ values
	*	\return predicted time modulations \f$ alpha\f$ for the ProMP
	*/
	double predictTimeMod(std::vector<Eigen::VectorXd> obsTraj, std::vector<ProMP> promp, double lb, double ub, double step);
	
	/** \brief				Infer which ProMP has to be associated to a given partial observation
	*	\param	obsTraj     observed (portion of) trajectory
	*	\param	listPromp   list of candidate ProMPs
	*	\param	infrdTimeMod   vector of inferred time modulations for each ProMP in listPromp
	*	\param	infr_threshold   classification threshold of each given motion. Observed trajectory has to be close to learned ProMP trajectory at least by this threshold
	*/
	int inferProMP(const Eigen::VectorXd &obsTraj, std::vector<ProMP> listPromp, const std::vector<double> &infrdTimeMod, const Eigen::VectorXd &infr_threshold);
	
/** \brief				Infer which ProMP has to be associated to a given partial observation
	*	\param	obsTraj     vector of observed (portions of) trajectories
	*	\param	listPromp   list of candidate ProMPs (a ProMp is a vector of 1d ProMps)
	*	\param	infrdTimeMod   vector of inferred time modulations for each ProMP in listPromp
	*	\param	infr_threshold   classification threshold of each given motion. Observed trajectory has to be close to learned ProMP trajectory at least by this threshold
	*/
	int inferProMP(std::vector<Eigen::VectorXd> obsTraj, std::vector<std::vector<ProMP>> listPromp, const std::vector<double> &infrdTimeMod, const Eigen::VectorXd &infr_threshold);
	
	/** \brief				 	Create a ProMP with updated parameters for the continuation of an observed movement
	*	\param	obsTraj      	observed (portion of) trajectory
	*	\param	infrdPromp   	ProMP object associated to the current observed trajectory
	*	\param	infrdTimeMod   	inferred time modulation for observed trajectory
	*	\return new ProMP object with updated mean weights and covariance and inferred time modulation
	*/
	ProMP predictFuture(const Eigen::VectorXd &obsTraj, ProMP infrdPromp, const double &infrdTimeMod);

private:
	
	//! expected noise in the trajectory observation
	double expNoise_;
	std::vector<double> distance_;
};
#endif
