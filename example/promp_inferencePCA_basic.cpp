/**
* Copyright: (C) 2020 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*
*
* In this example we encode the whole-body posture of the human with PCA
* We consider ony the first principal component (PC0)
* We learn the 1D promps related to the PC0 for three different (fast and very similar... challenging!) motions:
* right hook, right jab and right uppercut punches
* Given an observation of a punch we infer what is the most likely time modulation for each promp
* Then we infer which promp has to be associated to the current observation, i.e. which punch has to be associated to the observed portion of movement
*/


#include "promp.hpp"
#include "predictor.hpp"
#include "prompHandler.hpp"
#include <fstream>

namespace fs = boost::filesystem;
using namespace std;
const static IOFormat CSVFormat(FullPrecision, DontAlignCols, ", ", "\n");

int main(){
	// promp handler: num of demos (15), original data dimesnion (66 human joints), encoded dimension (5)
	PrompHandler ph(15,66,1);
	int n_rbf = 20;
	bool use_PCA = true;

	//--PCA-- 
	cout << "[INFO] Training PCA" << endl;
	// Load training dataset
	string full_path  = "../etc/";
	MatrixXd dataset = ph.loadPCADataset(full_path + "jMMA.csv");
	ph.findPCAEncoding(dataset);
	ph.loadPCA();

	//--PROMP-- 
	cout << "[INFO] Learning ProMp" << endl;
	//learn and store the encoded promps for a given motion (use folder name)
	std::vector<std::string> motions;
	motions.push_back("R_Jab");
	motions.push_back("R_Hook");
	motions.push_back("R_Uppercut");
	for (int i=0; i<motions.size(); i++){
		ph.encode_train(motions[i], "j");
		ph.learnEncodedPromp(motions[i],"j",n_rbf,1/(sqrt(n_rbf*n_rbf)));
	}
	//Load the 1D Promps of the first PC for the movements of interest (in your real-time module you just load them, if you learned them once before)
	ProMP rJab_promp = ph.getPromp("R_Jab","j",0,use_PCA);
	ProMP rHook_promp = ph.getPromp("R_Hook","j",0,use_PCA);
	ProMP rUppercut_promp = ph.getPromp("R_Uppercut","j",0,use_PCA);

	//--REAL-TIME INFERENCE/PREDICTION--
	// Load intended trajectory
	full_path = "../etc/demos/";
    CSVReader reader(full_path + "R_Uppercut/1.csv");
    Eigen::MatrixXd obsData = reader.getData();
    // transform in radians and encode with PCA
    obsData*=3.1415/180.0;
	Eigen::MatrixXd encoded_data = ph.encodew_PCA(obsData);
	// Observe only a portion of the 1st PC trajectory
	Eigen::VectorXd obsTraj = (encoded_data.col(0)).head(30);

    Predictor pred(0);
    double alpha_Jab = pred.predictTimeMod(obsTraj, rJab_promp, rJab_promp.getMeanDemoTimeMod()/2, rJab_promp.getMeanDemoTimeMod()*2, 0.1);
	cout << "PREDICTED_ALPHA Jab " << alpha_Jab << endl;
    double alpha_Hook = pred.predictTimeMod(obsTraj, rHook_promp, rHook_promp.getMeanDemoTimeMod()/2, rHook_promp.getMeanDemoTimeMod()*2, 0.1);
	cout << "PREDICTED_ALPHA Hook " << alpha_Hook << endl;
	double alpha_Up = pred.predictTimeMod(obsTraj, rUppercut_promp, rUppercut_promp.getMeanDemoTimeMod()/2, rUppercut_promp.getMeanDemoTimeMod()*2, 0.1);
	cout << "PREDICTED_ALPHA Uppercut " << alpha_Up << endl;

	std::vector<ProMP> candidatePromp;
	candidatePromp.push_back(rJab_promp);
	candidatePromp.push_back(rHook_promp);
	candidatePromp.push_back(rUppercut_promp);
	std::vector<double> alphas;
	alphas.push_back(alpha_Jab);
	alphas.push_back(alpha_Hook);
	alphas.push_back(alpha_Up);
	//setting high classification threshold, ignoring outliers
	Eigen::VectorXd thresh = Eigen::VectorXd::Ones(motions.size()*10000000);
	int PrompID = pred.inferProMP(obsTraj, candidatePromp, alphas, thresh);
	cout << "Inferred ProMP is: " << motions[PrompID] << endl;	

	Eigen::VectorXd meanTraj = rJab_promp.generateTrajectory(alpha_Jab);
	Eigen::VectorXd StdTraj = rJab_promp.genTrajStdDev();

	Eigen::VectorXd meanTraj1 = rHook_promp.generateTrajectory(alpha_Hook);
	Eigen::VectorXd StdTraj1 = rHook_promp.genTrajStdDev();

	Eigen::VectorXd meanTraj2 = rUppercut_promp.generateTrajectory(alpha_Up);
	Eigen::VectorXd StdTraj2 = rUppercut_promp.genTrajStdDev();

	// Below code is for writing all the data into an output.csv file for visualization.
	cout << "... Saving data" << endl;
	std::ofstream myfile;

	myfile.open ("Jab_infrd.csv");
	for (int i=0;i<meanTraj.size();++i){
		myfile <<meanTraj(i)<<","<<StdTraj(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("Hook_infrd.csv");
	for (int i=0;i<meanTraj1.size();++i){
		myfile <<meanTraj1(i)<<","<<StdTraj1(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("Uppercut_infrd.csv");
	for (int i=0;i<meanTraj2.size();++i){
		myfile <<meanTraj2(i)<<","<<StdTraj2(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("obs.csv");
	myfile <<encoded_data.col(0)<<std::endl;
	myfile.close();
	cout << "Saved data" << endl;

	return 0;
}
