/**
* Copyright: (C) 2020 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*
*
* In this example we consider several movements represented by a 1D trajectory
* We learn the 1D promps associated to those movemnts
* In this case the movemnets are: right hook, right jab and right uppercut punches. For each we consider the trajectory of the shoulder roll.
* Given an observation of a punch we infer what is the most likely time modulation for each promp
* Then we infer which promp has to be associated to the current observation, i.e. which punch has to be associated to the observed portion of movement
* Finally we predict the future evolution of the movement given the observation
*/


#include "promp.hpp"
#include "predictor.hpp"
#include "prompHandler.hpp"
#include <fstream>

namespace fs = boost::filesystem;
using namespace std;
const static IOFormat CSVFormat(FullPrecision, DontAlignCols, ", ", "\n");

int main(){
	// promp handler: num of demos (10)
	PrompHandler ph(10);
	int n_rbf = 20;
	bool use_PCA = false;
	int obs_duration =70;

	//--PROMP-- 
	cout << "[INFO] Learning ProMp" << endl;
	std::vector<std::string> motions;
	motions.push_back("R_Hook");
	motions.push_back("R_Jab");
	motions.push_back("R_Uppercut");
	std::vector<ProMP> candidatePromp;
	std::vector<double> threshold_vec;
	for (int m=0; m<motions.size(); m++){
		//learn and store the 1D Promps of the 21st joint (shoulder roll) for a given motion (use folder name)
		ph.learnPromp(motions[m],"j",21,n_rbf,1/(sqrt(n_rbf*n_rbf)));
		//Load the 1D Promps of the 21st joint (shoulder roll) for the movements of interest (in your real-time module you just load them, if you learned them once before)
		candidatePromp.push_back(ph.getPromp(motions[m],"j",21,use_PCA));
		//get classification threshold vector
		threshold_vec.push_back(ph.computeMotionThreshold(motions[m],"j",21,use_PCA,obs_duration));
	}
	ph.saveMotionsThreshold(threshold_vec);
	Eigen::VectorXd prompID_threshold = ph.getMotionsThreshold();

	//--REAL-TIME INFERENCE/PREDICTION--
	// Load intended trajectory
	stringstream filePath;
	fs::path pathfs = fs::current_path();
	string path = pathfs.string();
	filePath  << path <<  "/../etc/demos/";
	string full_path  = filePath.str();
    CSVReader reader(full_path + "OpenDoor/1.csv");
    Eigen::MatrixXd obsData = reader.getData();
    // Observe only a portion of the the intended trajectory
    Eigen::VectorXd obsTraj = (obsData.col(21)).head(obs_duration);

    Predictor pred(0);
    std::vector<double> alphas;
    for (int i=0; i<candidatePromp.size(); i++){
    	alphas.push_back(pred.predictTimeMod(obsTraj, candidatePromp[i], candidatePromp[i].getMeanDemoTimeMod()/4, candidatePromp[i].getMeanDemoTimeMod()*2, 0.1));
    	cout << "PREDICTED_ALPHA " << motions[i] << ": " << alphas[i] << endl;
    }

	int PrompID = pred.inferProMP(obsTraj, candidatePromp, alphas, prompID_threshold);
	cout << "Inferred ProMP is: " << motions[PrompID] << endl;

	ProMP fut = pred.predictFuture(obsTraj, candidatePromp[PrompID], alphas[PrompID]);

	// Mean and std trajectories of variations
	Eigen::VectorXd meanTraj = candidatePromp[PrompID].generateTrajectory(alphas[PrompID]);
	double zero = meanTraj(0);
	for (int i=0; i<meanTraj.size(); i++)
		meanTraj(i)-=zero;
	Eigen::VectorXd StdTraj = candidatePromp[PrompID].genTrajStdDev();
	
	Eigen::VectorXd meanTraj1 = fut.generateTrajectory();
	zero = meanTraj1(0);
	for (int i=0; i<meanTraj1.size(); i++)
		meanTraj1(i)-=zero;
	Eigen::VectorXd StdTraj1 = fut.genTrajStdDev();
	
	fut.addViaPoint(fut.getTrajLength()-1, 20+zero, 0.000001);
	fut.conditionViaPoints();
	Eigen::VectorXd meanTraj2 = fut.generateTrajectory();
	for (int i=0; i<meanTraj2.size(); i++)
		meanTraj2(i)-=zero;
	Eigen::VectorXd StdTraj2 = fut.genTrajStdDev();


	// Below code is for writing all the data into an output.csv file for visualization.
	cout << "... Saving data" << endl;
	std::ofstream myfile;

	myfile.open ("learned.csv");
	for (int i=0;i<meanTraj.size();++i){
		myfile <<meanTraj(i)<<","<<StdTraj(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("future.csv");
	for (int i=0;i<meanTraj1.size();++i){
		myfile <<meanTraj1(i)<<","<<StdTraj1(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("condition.csv");
	for (int i=0;i<meanTraj2.size();++i){
		myfile <<meanTraj2(i)<<","<<StdTraj2(i)<<std::endl;
	}
	myfile.close();

	Eigen::VectorXd zeros=Eigen::VectorXd::Ones(obsData.rows());
	zeros*=(obsData.col(21))(0);
	myfile.open ("obs.csv");
	myfile <<obsData.col(21)-zeros<<std::endl;
	//myfile <<obsData.col(21)<<std::endl;
	myfile.close();
	cout << "Saved data" << endl;

	return 0;
}