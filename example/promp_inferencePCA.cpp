/**
* Copyright: (C) 2020 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*
*
* In this example we encode the whole-body posture of the human with PCA
* We consider the first 3 principal components (PC)
* We learn the 1D promps related to the 3 PC for different motions
* Given an observation of the first PC we infer what is the most likely time modulation for each promp
* Then we infer which promp has to be associated to the current observation of the first 3 PC
*/


#include "promp.hpp"
#include "predictor.hpp"
#include "prompHandler.hpp"
#include <fstream>

namespace fs = boost::filesystem;
using namespace std;
const static IOFormat CSVFormat(FullPrecision, DontAlignCols, ", ", "\n");

int main(){
	// promp handler: num of demos (10), original data dimesnion (66 human joints), encoded dimension (6)
	int encoded_dim = 3;
	PrompHandler ph(10,66,encoded_dim);
	int n_rbf = 20;
	bool use_PCA = true;
	int obs_duration = 200;

	//--PCA-- 
	cout << "[INFO] Training PCA" << endl;
	// Load training dataset
	string full_path  = "../etc/";
	MatrixXd dataset = ph.loadPCADataset(full_path + "jSC.csv");
	ph.findPCAEncoding(dataset);
	ph.loadPCA();

	//--PROMP-- 
	cout << "[INFO] Learning ProMp" << endl;
	std::vector<std::string> motions;
	motions.push_back("OpenDoor");
	motions.push_back("OpenDrawer");
	motions.push_back("PickupBox");
	motions.push_back("PutasideBox");
	motions.push_back("ReachDoor");
	motions.push_back("ReachDrawer");
	motions.push_back("ReachLED");
	
	std::vector<ProMP> promp_vec, promp_vec1, promp_vec2;
	std::vector<double> threshold_vec, threshold_vec1, threshold_vec2;
	for (int i=0; i<motions.size(); i++){
		// //encode training data for promps
		ph.encode_train(motions[i], "j");
		//learn and store the encoded promps for a given motion (use folder name)
		ph.learnEncodedPromp(motions[i],"j",n_rbf,1/(sqrt(n_rbf*n_rbf)));
		//Load the 1D Promps of the first PC for the movements of interest (in your real-time module you just load them, if you learned them once before)
		promp_vec.push_back(ph.getPromp(motions[i],"j",0,use_PCA));
		promp_vec1.push_back(ph.getPromp(motions[i],"j",1,use_PCA));
		promp_vec2.push_back(ph.getPromp(motions[i],"j",2,use_PCA));
		//get classification threshold vector
		threshold_vec.push_back(ph.computeMotionThreshold(motions[i],"j",0,use_PCA,obs_duration));
		threshold_vec1.push_back(ph.computeMotionThreshold(motions[i],"j",1,use_PCA,obs_duration));
		threshold_vec2.push_back(ph.computeMotionThreshold(motions[i],"j",2,use_PCA,obs_duration));
	}
	for (int i=0; i<motions.size(); i++)
		threshold_vec[i]+=threshold_vec1[i]+threshold_vec2[i];
	ph.saveMotionsThreshold(threshold_vec);
	Eigen::VectorXd prompID_threshold = ph.getMotionsThreshold();

	std::cout << prompID_threshold << std::endl;
	//--REAL-TIME INFERENCE/PREDICTION--
	// Load intended trajectory
	full_path = "../etc/demos/";
    CSVReader reader(full_path + "ReachLED/9.csv");
    Eigen::MatrixXd obsData = reader.getData();
    // transform in radians and encode with PCA
    obsData*=3.1415/180.0;
	Eigen::MatrixXd encoded_data = ph.encodew_PCA(obsData);
	// Observe only a portion of the PC trajectory=ies
	if(obs_duration>(encoded_data.col(0)).size())
		obs_duration = (encoded_data.col(0)).size();
	Eigen::VectorXd obsTraj = (encoded_data.col(0)).head(obs_duration);
	Eigen::VectorXd obsTraj1 = (encoded_data.col(1)).head(obs_duration);
	Eigen::VectorXd obsTraj2 = (encoded_data.col(2)).head(obs_duration);
	std::vector<Eigen::VectorXd> obsTraj_vec;
	obsTraj_vec.push_back(obsTraj);
	obsTraj_vec.push_back(obsTraj1);
	obsTraj_vec.push_back(obsTraj2);

	cout << "START PREDICTION " << endl;
    Predictor pred(0);
    std::vector<double> alphas;
    for (int i=0; i<motions.size(); i++){
    	std::vector<ProMP> promp_enc;
    	promp_enc.push_back(promp_vec[i]);
    	promp_enc.push_back(promp_vec1[i]);
    	promp_enc.push_back(promp_vec2[i]);
		alphas.push_back(pred.predictTimeMod(obsTraj_vec, promp_enc, promp_vec[i].getMeanDemoTimeMod()/4, promp_vec[i].getMeanDemoTimeMod(), 0.1));
	}
	cout << "PREDICTED_ALPHA " << motions[0] << ": " << alphas[0] << endl;
	cout << "PREDICTED_ALPHA " << motions[1] << ": " << alphas[1] << endl;
	cout << "PREDICTED_ALPHA " << motions[2] << ": " << alphas[2] << endl;

	std::vector<std::vector<ProMP>> promp_enc_vec;
	promp_enc_vec.push_back(promp_vec);
	promp_enc_vec.push_back(promp_vec1);
	promp_enc_vec.push_back(promp_vec2);
	int PrompID = pred.inferProMP(obsTraj_vec, promp_enc_vec, alphas, prompID_threshold);
	if(PrompID!=-1)
		cout << "Inferred ProMP is: " << motions[PrompID] << endl;	
	else
		cout << "No classification" << endl;	

	Eigen::VectorXd meanTraj = promp_vec[0].generateTrajectory(alphas[0]);
	Eigen::VectorXd StdTraj = promp_vec[0].genTrajStdDev();

	Eigen::VectorXd meanTraj1 = promp_vec[1].generateTrajectory(alphas[1]);
	Eigen::VectorXd StdTraj1 = promp_vec[1].genTrajStdDev();

	Eigen::VectorXd meanTraj2 = promp_vec[2].generateTrajectory(alphas[2]);
	Eigen::VectorXd StdTraj2 = promp_vec[2].genTrajStdDev();

	Eigen::VectorXd meanTraj_ = promp_vec1[0].generateTrajectory(alphas[0]);
	Eigen::VectorXd StdTraj_ = promp_vec1[0].genTrajStdDev();

	Eigen::VectorXd meanTraj1_ = promp_vec1[1].generateTrajectory(alphas[1]);
	Eigen::VectorXd StdTraj1_ = promp_vec1[1].genTrajStdDev();

	Eigen::VectorXd meanTraj2_ = promp_vec1[2].generateTrajectory(alphas[2]);
	Eigen::VectorXd StdTraj2_ = promp_vec1[2].genTrajStdDev();

	// Below code is for writing all the data into an output.csv file for visualization.
	cout << "... Saving data" << endl;
	std::ofstream myfile;

	myfile.open ("1_infrd.csv");
	for (int i=0;i<meanTraj.size();++i){
		myfile <<meanTraj(i)<<","<<StdTraj(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("2_infrd.csv");
	for (int i=0;i<meanTraj1.size();++i){
		myfile <<meanTraj1(i)<<","<<StdTraj1(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("3_infrd.csv");
	for (int i=0;i<meanTraj2.size();++i){
		myfile <<meanTraj2(i)<<","<<StdTraj2(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("1_infrd_.csv");
	for (int i=0;i<meanTraj_.size();++i){
		myfile <<meanTraj_(i)<<","<<StdTraj_(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("2_infrd_.csv");
	for (int i=0;i<meanTraj1_.size();++i){
		myfile <<meanTraj1_(i)<<","<<StdTraj1_(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("3_infrd_.csv");
	for (int i=0;i<meanTraj2_.size();++i){
		myfile <<meanTraj2_(i)<<","<<StdTraj2_(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("obs.csv");
	myfile <<encoded_data.col(encoded_dim-2)<<std::endl;
	myfile.close();

	myfile.open ("obs_.csv");
	myfile <<encoded_data.col(encoded_dim-1)<<std::endl;
	myfile.close();
	cout << "Saved data" << endl;

	return 0;
}
