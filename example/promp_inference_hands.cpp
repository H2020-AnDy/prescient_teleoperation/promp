/**
* Copyright: (C) 2020 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*
*
*/


#include "promp.hpp"
#include "predictor.hpp"
#include "prompHandler.hpp"
#include <fstream>

namespace fs = boost::filesystem;
using namespace std;
const static IOFormat CSVFormat(FullPrecision, DontAlignCols, ", ", "\n");

int main(){
	// promp handler: num of demos (10)
	PrompHandler ph(10);
	int n_rbf = 20;
	bool use_PCA = false;
	int obs_duration = 40;

	//--PROMP-- 
	cout << "[INFO] Learning ProMp" << endl;
	std::vector<std::string> motions;
	motions.push_back("OpenDoor");
	motions.push_back("ReachMixed");
	motions.push_back("PutasideBox");
	motions.push_back("PickupBox");
	motions.push_back("OpenDrawer");
	motions.push_back("PutupBox");

	Eigen::VectorXi pos_id_(4);
	// id of position idx from xsens poseQuaternion bottle
	pos_id_<< 2,70,71,72;
	
	std::vector<ProMP> promp_vec, promp_vec1, promp_vec2;
	std::vector<double> threshold_vec, threshold_vec1, threshold_vec2;
	for (int i=0; i<motions.size(); i++){
		//learn and store the promps for a given motion (use folder name)
		ph.learnPromp(motions[i],"p",pos_id_,n_rbf,1/(sqrt(n_rbf*n_rbf)));
		//Load the 1D Promps of the right hand coordinates for the movements of interest (in your real-time module you just load them, if you learned them once before)
		promp_vec.push_back(ph.getPromp(motions[i],"p",pos_id_(3),use_PCA));
		promp_vec1.push_back(ph.getPromp(motions[i],"p",pos_id_(2),use_PCA));
		promp_vec2.push_back(ph.getPromp(motions[i],"p",pos_id_(1),use_PCA));
		//get classification threshold vector
		threshold_vec.push_back(ph.computeMotionThreshold(motions[i],"p",pos_id_(3),use_PCA,obs_duration));
		threshold_vec1.push_back(ph.computeMotionThreshold(motions[i],"p",pos_id_(2),use_PCA,obs_duration));
		threshold_vec2.push_back(ph.computeMotionThreshold(motions[i],"p",pos_id_(1),use_PCA,obs_duration));
	}
	for (int i=0; i<motions.size(); i++)
		threshold_vec[i]+=threshold_vec1[i]+threshold_vec2[i];
	ph.saveMotionsThreshold(threshold_vec);
	Eigen::VectorXd prompID_threshold = ph.getMotionsThreshold();

	//--REAL-TIME INFERENCE/PREDICTION--
	// Load intended trajectory
	string full_path = "../etc/demos/";
    CSVReader reader(full_path + "ReachMixed/p2.csv");
    Eigen::MatrixXd obsData = reader.getData();
	// Observe only a portion of the hand trajectories
	if(obs_duration>(obsData.col(0)).size())
		obs_duration = (obsData.col(0)).size();
	Eigen::VectorXd obsTraj = (obsData.col(pos_id_(3))).head(obs_duration);
	Eigen::VectorXd obsTraj1 = (obsData.col(pos_id_(2))).head(obs_duration);
	Eigen::VectorXd obsTraj2 = (obsData.col(pos_id_(1))).head(obs_duration);
	std::vector<Eigen::VectorXd> obsTraj_vec;
	obsTraj_vec.push_back(obsTraj);
	obsTraj_vec.push_back(obsTraj1);
	obsTraj_vec.push_back(obsTraj2);


	cout << "START PREDICTION " << endl;
    Predictor pred(0);
    std::vector<double> alphas;
    for (int i=0; i<motions.size(); i++){
    	std::vector<ProMP> promp_enc;
    	promp_enc.push_back(promp_vec[i]);
    	promp_enc.push_back(promp_vec1[i]);
    	promp_enc.push_back(promp_vec2[i]);
		alphas.push_back(pred.predictTimeMod(obsTraj_vec, promp_enc, promp_vec[i].getMeanDemoTimeMod()/4, promp_vec[i].getMeanDemoTimeMod()*2, 0.1));
	}
	cout << "PREDICTED_ALPHA " << motions[0] << ": " << alphas[0] << endl;
	cout << "PREDICTED_ALPHA " << motions[1] << ": " << alphas[1] << endl;
	cout << "PREDICTED_ALPHA " << motions[2] << ": " << alphas[2] << endl;

	std::vector<std::vector<ProMP>> promp_enc_vec;
	promp_enc_vec.push_back(promp_vec);
	promp_enc_vec.push_back(promp_vec1);
	promp_enc_vec.push_back(promp_vec2);
	int PrompID = pred.inferProMP(obsTraj_vec, promp_enc_vec, alphas, prompID_threshold);
	if(PrompID!=-1)
		cout << "Inferred ProMP is: " << motions[PrompID] << endl;	
	else
		cout << "No classification" << endl;	

	Eigen::VectorXd meanTraj = promp_vec[0].generateTrajectory(alphas[0]);
	Eigen::VectorXd StdTraj = promp_vec[0].genTrajStdDev();

	Eigen::VectorXd meanTraj1 = promp_vec[1].generateTrajectory(alphas[1]);
	Eigen::VectorXd StdTraj1 = promp_vec[1].genTrajStdDev();

	Eigen::VectorXd meanTraj2 = promp_vec[2].generateTrajectory(alphas[2]);
	Eigen::VectorXd StdTraj2 = promp_vec[2].genTrajStdDev();

	Eigen::VectorXd meanTraj_ = promp_vec1[0].generateTrajectory(alphas[0]);
	Eigen::VectorXd StdTraj_ = promp_vec1[0].genTrajStdDev();

	Eigen::VectorXd meanTraj1_ = promp_vec1[1].generateTrajectory(alphas[1]);
	Eigen::VectorXd StdTraj1_ = promp_vec1[1].genTrajStdDev();

	Eigen::VectorXd meanTraj2_ = promp_vec1[2].generateTrajectory(alphas[2]);
	Eigen::VectorXd StdTraj2_ = promp_vec1[2].genTrajStdDev();

	// Below code is for writing all the data into an output.csv file for visualization.
	cout << "... Saving data" << endl;
	std::ofstream myfile;

	myfile.open ("1_infrd.csv");
	for (int i=0;i<meanTraj.size();++i){
		myfile <<meanTraj(i)<<","<<StdTraj(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("2_infrd.csv");
	for (int i=0;i<meanTraj1.size();++i){
		myfile <<meanTraj1(i)<<","<<StdTraj1(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("3_infrd.csv");
	for (int i=0;i<meanTraj2.size();++i){
		myfile <<meanTraj2(i)<<","<<StdTraj2(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("1_infrd_.csv");
	for (int i=0;i<meanTraj_.size();++i){
		myfile <<meanTraj_(i)<<","<<StdTraj_(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("2_infrd_.csv");
	for (int i=0;i<meanTraj1_.size();++i){
		myfile <<meanTraj1_(i)<<","<<StdTraj1_(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("3_infrd_.csv");
	for (int i=0;i<meanTraj2_.size();++i){
		myfile <<meanTraj2_(i)<<","<<StdTraj2_(i)<<std::endl;
	}
	myfile.close();

	myfile.open ("obs.csv");
	myfile <<obsData.col(pos_id_(3))<<std::endl;
	myfile.close();

	myfile.open ("obs_.csv");
	myfile <<obsData.col(pos_id_(2))<<std::endl;
	myfile.close();
	cout << "Saved data" << endl;

	return 0;
}
