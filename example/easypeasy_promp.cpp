/**
* Copyright: (C) 2019 Inria
* Author: Luigi Penco
* CopyPolicy: Released under the terms of the GNU GPL v2.0.
*
*
* Learning and plotting a 1D ProMP
*/


#include "promp.hpp"
#include "predictor.hpp"
#include "prompHandler.hpp"
#include <fstream>

using namespace std;

int main(){
	// promp handler: num of demos (10)
	PrompHandler ph(10);
	int n_rbf = 20;
	bool use_PCA = false;

	//--PROMP--
	//learn and store the 1D Promps of the 3rd body segment (neck) for a given motion (use folder name)
	// "p" for body segments pose or "j" for joints
	ph.learnPromp("ReachDoor","p",3,n_rbf,1/sqrt(n_rbf*n_rbf));
	//Load the 1D Promps of the 3rd body segment (neck) for the movements of interest (in your real-time module you just load them, if you learned them once before)
	ProMP my_promp = ph.getPromp("ReachDoor","p",3,use_PCA);

	Eigen::VectorXd mTraj = my_promp.generateTrajectory(1.0);
	Eigen::VectorXd stdTraj = my_promp.genTrajStdDev();
	
	/// Below code is for writing all the data into an output.csv file for visualization.
	std::ofstream myfile;
	myfile.open ("my_promp.csv");
	for (int i=0;i<mTraj.size();++i){
		myfile <<mTraj(i)<<","<<stdTraj(i)<<std::endl;
	}
	myfile.close();

	return 0;
}
