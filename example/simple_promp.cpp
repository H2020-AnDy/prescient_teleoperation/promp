#include "promp.hpp"
#include "prompHandler.hpp"
#include <fstream>

int main(){
	/**
	* vector of strings containing file names of all .csv files (each .csv file contains data from a single demonstration)
	*/
	std::vector<std::string> fileList;
	std::string filePath = "../etc/demos/PickupBox/";
	fileList.push_back(filePath + "p1.csv");
	fileList.push_back(filePath + "p2.csv");
	fileList.push_back(filePath + "p3.csv");
	fileList.push_back(filePath + "p4.csv");
	fileList.push_back(filePath + "p5.csv");
	fileList.push_back(filePath + "p6.csv");
	fileList.push_back(filePath + "p7.csv");
	fileList.push_back(filePath + "p8.csv");
	fileList.push_back(filePath + "p9.csv");
	fileList.push_back(filePath + "p10.csv");
	/**
	* data handle object which takes an index of trajectory in .csv file and also the file list
	*/
	DataHandle dh(72,fileList);
	/// get required trajectory from all demonstrations into a single matrix
	Eigen::MatrixXd data = dh.getData();	
	/// initialize promp object with number of basis functions and std as arguments.
	int n_rbf = 20;
	ProMP m_promp(data,n_rbf,1/(sqrt(n_rbf*n_rbf)));
	/// generate trajectory of required number of points with the generateTrajectory function.
	// Eigen::VectorXd weights(5);
	// weights << 1.06 ,1.09, 0.76, 1.06, 1.07; 
	// Eigen::VectorXd vect = m_promp.generateTrajectory(weights,10);
	Eigen::VectorXd mTraj = m_promp.generateTrajectory(1.0);
	Eigen::VectorXd stdTraj = m_promp.genTrajStdDev();
	// m_promp.addViaPoint(data.cols(), 40, 0.000001);
	// // m_promp.addViaPoint(0, mTraj(0), 0.000001);
	// m_promp.conditionViaPoints();
	// mTraj = m_promp.generateTrajectory(2.0);
	// stdTraj = m_promp.genTrajStdDev();

	/// Below code is for writing all the data into an output.csv file for visualization.
	std::ofstream myfile;
	myfile.open ("my_promp.csv");
	int j;
	int k;
	int plotlen;
	if (mTraj.innerSize()>data.outerSize())
		plotlen =mTraj.innerSize();
	else
		plotlen =data.outerSize();
	for (int i=0;i<plotlen;++i){
		if (i<data.outerSize())
			j=i;
		else 
			j=data.outerSize()-1;

		if (i<mTraj.innerSize())
			k=i;
		else 
			k=mTraj.innerSize()-1;
		myfile << data(0,j)<<","<<data(1,j)<<","<< data(2,j)<<","<< data(3,j)<<","<<data(4,j)<<","<< data(5,j)<<","<< data(6,j)<<","<<data(7,j)<<","<< data(8,j)<<","<< data(9,j)<<","<<mTraj(k)<<","<<stdTraj(k)<<std::endl;
	}
	myfile.close();
	return 0;
}